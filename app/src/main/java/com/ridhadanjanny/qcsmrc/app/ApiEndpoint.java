package com.ridhadanjanny.qcsmrc.app;

public class ApiEndpoint {

    public static final String BASE_URL_DOWNLOAD = "http://mazhtersevents.com";

    // BASE URL
    public static final String BASE_URL = "http://mazhtersevents.com/emonev/api_qc_smrc";

    // BASE URL (WILAYAH : PROVINSI, DAPIL, DAN KAB KOTA)
    public static final String BASE_URL_WILAYAH = "http://mazhtersevents.com/emonev/api_qc";
    // ENDPOINT WILAYAH
    public static final String BASE_URL_WILAYAH_PROVINSI = BASE_URL_WILAYAH + "/get_provinsi";
    public static final String BASE_URL_WILAYAH_DAPIL = BASE_URL_WILAYAH + "/get_dapil";
    public static final String BASE_URL_WILAYAH_KABKOTA = BASE_URL_WILAYAH + "/get_kabkota";

    // ENDPOINT INISIASI
    public static final String POST_SUBMIT_INISIASI_RELAWAN = BASE_URL + "/submit_inisiasi";
    public static final String POST_UPDATE_INISIASI_RELAWAN = BASE_URL + "/update_inisiasi_new";

    // ENDPOINT GET ALL CALEG
    public static final String GET_LIST_DATA_CALEG = BASE_URL + "/get_caleg_by_dapil";


    public static final String GET_LIST_TYPE_RELAWAN = BASE_URL + "/list_type_relawan";
    public static final String GET_LIST_TYPE_CALEG = BASE_URL + "/list_type_caleg";
    public static final String GET_LIST_DAPIL = BASE_URL + "/list_dapil";
    public static final String GET_LIST_CALEG = BASE_URL + "/list_caleg";
    public static final String POST_SUBMIT = BASE_URL + "/submit_data";

    // QC PEMILU ENDPOINT
    public static final String POST_INSERT_INISIASI = BASE_URL + "/insert_inisiasi";

    //TAG
    public static final String DEFAULT_TAG    = "qcsmrc_tag";

    public static final String POST_SUBMIT_VOTE_CALEG_RESULT = BASE_URL + "/post_vote_caleg";

    // http://mazhtersevents.com/emonev/exit_poll_pemilu.zip
    public static final String DOWNLOAD_FILE_EXIT_POLL =
            BASE_URL_DOWNLOAD + "/emonev/exit_poll_pemilu_smrc.zip";

}
