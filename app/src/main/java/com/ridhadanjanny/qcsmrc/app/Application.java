package com.ridhadanjanny.qcsmrc.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.ridhadanjanny.qcsmrc.model.User;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import me.yokeyword.fragmentation.Fragmentation;
import me.yokeyword.fragmentation.helper.ExceptionHandler;

public class Application extends android.app.Application {

    private static Application mInstance;
    private PreferenceManager pref;

    private User mUser;

    private SmsBroadcastReceiver smsBroadcastReceiver;

    public static synchronized Application getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .name("qc.realm")
                .build();

        Realm.setDefaultConfiguration(config);

        AndroidNetworking.initialize(getApplicationContext());

        Fragmentation.builder()
                .stackViewMode(Fragmentation.BUBBLE)
                .debug(false)
                .handleException(new ExceptionHandler() {
                    @Override
                    public void onException(Exception e) {
                        // 以Bugtags为例子: 把捕获到的 Exception 传到 Bugtags 后台。
                        // Bugtags.sendException(e);
                    }
                })
                .install();

        /**
         * SMS 1 : KIRIM SMS JUMLAH QC KE GATEWAY
         */

        // receive sms from gateway, and send back `OK` to gateway
        receiveSmsFromGateway();

        // handle SMS report status `SENT`
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        showToast("SMS 1 : SMS Sent - Format QC");
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        showToast("Generic Failure");
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        showToast("No Service");
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        showToast("Null PDU");
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        showToast("Radio off");
                        break;
                    default:
                        break;
                }
            }
        }, new IntentFilter(Config.SENT));

        // handle SMS report status `DELIVERED`
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        showToast("SMS 1 : SMS Delivered - Format QC");
                        break;
                    case Activity.RESULT_CANCELED:
                        showToast("SMS 1 : SMS Not Delivered - Format QC");
                        break;
                }
            }
        }, new IntentFilter(Config.DELIVERED));


        /**
         * SMS 2 : KIRIM SMS BALASAN `OK` KE GATEWAY
         */

        // handle SMS report status `OK`
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        showToast("SMS 2 : SMS Delivered - OK");
                        break;
                    case Activity.RESULT_CANCELED:
                        showToast("SMS 2 : SMS Not Delivered - OK");
                        break;
                }
            }
        }, new IntentFilter(Config.DELIVERED));


    }

    private void showToast(String message) {
        Toast.makeText(mInstance,
                message
                , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(smsBroadcastReceiver);
        super.onTerminate();
    }

    /**
     * Get Shared Preference object to manipulate entity
     */
    public PreferenceManager getPrefManager() {
        if(pref == null) {
            pref = new PreferenceManager(this);
        }
        return pref;
    }

    private void receiveSmsFromGateway() {

        // receive sms from broadcast receiever (96999)
            smsBroadcastReceiver = new SmsBroadcastReceiver(Config.SHORT_NUMBER, "[Rp0]");
            registerReceiver(smsBroadcastReceiver, new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION));
            smsBroadcastReceiver.setListener(new SmsBroadcastReceiver.Listener() {
                @Override
                public void onTextReceived(String text) {
//                Log.v("Application", text);
//                Toast.makeText(mInstance, text, Toast.LENGTH_SHORT).show();
                }
            });

    }
}
