package com.ridhadanjanny.qcsmrc.app;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Config class for store key of shared preference file name
 */
public class Config {

    // FORMAT SMS
    public static final String ENTITY = "SMRC";
    public static final String QC_PRESIDEN = "P";
    public static final String QC_PARTAI = "L";

    public static final String SENT = "SENT";
    public static final String DELIVERED = "DELIVERED";

    // +6285714823418
    public static final String LONG_NUMBER = "+6285714823418";
    public static final String SHORT_NUMBER = "96999";

    // USER SHARED KEY
    public static final String SHARED_PREF_USER = "user_info";
    public static final String KEY_USER_EDITED = "user_edited";
    public static final String KEY_USER_SUBMITTED = "user_submitted";
    public static final String KEY_USER_SUBMITTED_PARTAI = "user_submitted_partai";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_KODE_RELAWAN = "user_kode_relawan";
    public static final String KEY_USER_NAME = "user_nama";
    public static final String KEY_TIPE_RELAWAN = "user_tipe_relawan";
    public static final String KEY_USER_HP_1 = "user_no_hp1";
    public static final String KEY_USER_HP_2 = "user_no_hp2";
    public static final String KEY_USER_ACCESS_FORM_CALEG = "user_access_form_caleg";

    // USER SHARED KEY - WILAYAH
    public static final String KEY_USER_PROV = "user_prov";
    public static final String KEY_USER_DAPIL = "user_dapil";
    public static final String KEY_USER_KABKOTA = "user_kabkota";

    // long number
    public static final String KEY_USER_LONG_NUMBER = "user_long_number";

    /**
     * Get data of tipe relawan in Map
     * @return HashMap<String,Integer>
     */
    public static HashMap<String, Integer> getMapDataTipeRelawan() {
        HashMap<String, Integer> data = new HashMap<String, Integer>();
        data.put("Enumerator", 1);
        data.put("Spotchecker", 2);
        return data;
    }

    /**
     * Get kode sms (Q or S) of tipe relawan in Map
     * @return HashMap<String,Integer>
     */
    public static HashMap<String, String> getKodeSmsTipeRelawan() {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("Enumerator", "Q");
        data.put("Spotchecker", "S");
        return data;
    }

    /**
     * Get data of tipe relawan in ArrayList
     * @return ArrayList<String>
     */
    public static ArrayList<String> getListDataTipeRelawan() {
        ArrayList<String> data = new ArrayList<String>();
        data.add("Enumerator");
        data.add("Spotchecker");
        return data;
    }

    /**
     * Get data of partai in ArrayList
     * @return ArrayList<String>
     */
    public static ArrayList<String> getPartai() {
        ArrayList<String> data = new ArrayList<String>();
        data.add("1 PKB");
        data.add("2 Gerindra");
        data.add("3 PDI Perjuangan");
        data.add("4 Golkar");
        data.add("5 Nasdem");
        data.add("6 Garuda");
        data.add("7 Berkarya");
        data.add("8 PKS");
        data.add("9 Perindo");
        data.add("10 PPP");
        data.add("11 PSI");
        data.add("12 PAN");
        data.add("13 Hanura");
        data.add("14 Demokrat");
        data.add("19 PBB");
        data.add("20 PKPI");
        data.add("Surat Tidak Sah");
        data.add("DPT");
        return data;
    }

}
