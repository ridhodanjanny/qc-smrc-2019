package com.ridhadanjanny.qcsmrc.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.utils.Utils;
import com.ridhadanjanny.qcsmrc.BuildConfig;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.ApiEndpoint;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import ir.mahdi.mzip.zip.ZipArchive;

public class ExitPollListener {

    final private Context mContext;
    private String mPackageName;
    private PackageManager mPackageManager;
    private String mCsEntryPackageName = "gov.census.cspro.csentry";

    private MaterialDialog mDialog;
    private final String TAG = ExitPollListener.class.getSimpleName();

    public ExitPollListener(Context context, String packageName, PackageManager packageManager) {
        mContext = context;
        mPackageName = packageName;
        mPackageManager = packageManager;
    }

    /**
     * Install CS Entry
     * - Check if package is installed before, launch the app
     * - if app not installed, download app by url
     */
    public void installExitPollApp() {
        if (!isPackageInstalled(mPackageName)) {
            downloadFileOkHttp(ApiEndpoint.DOWNLOAD_FILE_EXIT_POLL, "exitPollPackageSmrc.zip");
        } else {
            // if cs entry belum terinstall, install dulu
            if (!isPackageInstalled(mCsEntryPackageName)) {
                // install cs entry
                String[] appNameList = {"csentry.apk"};
                installAllAppExitPoll(appNameList);
            } else {
                Intent intentToApp = mPackageManager.getLaunchIntentForPackage(mCsEntryPackageName);
                mContext.startActivity(intentToApp);
            }
        }
    }

    /**
     * Download file zip for install necessary app
     *
     * @param downloadFileExitPoll
     * @param fileName
     */
    private void downloadFileOkHttp(String downloadFileExitPoll, final String fileName) {

        mDialog = new MaterialDialog.Builder(mContext)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();

        AndroidNetworking.download(downloadFileExitPoll, dirPath, fileName)
                .setTag("downloadTest")
                .setPriority(Priority.MEDIUM)
                .build()
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
//                        showLog(String.valueOf(totalBytes));
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        File csEntryDir = new File(dirPath + "/csentry");
                        csEntryDir.mkdirs();

                        ZipArchive zipArchive = new ZipArchive();
                        String fileZipPath = dirPath + "/" + fileName; // zip path
                        String csEntryPath = dirPath + "/csentry"; // extract to folder /csentry

                        // unzip csentry, dapil uploader, & config file
                        zipArchive.unzip(fileZipPath, csEntryPath, "");

                        mDialog.cancel();

                        // install CS Entry & Dapil Uploader
                        String[] appNameList = {"csentry.apk", "expol_april19.apk"};
                        installAllAppExitPoll(appNameList);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        mDialog.cancel();
                        showToast("Download Error!\n" + error.getErrorDetail());
                    }
                });
    }

    /**
     * Check if app is installed before
     *
     * @return boolean
     */
    private boolean isPackageInstalled(String packageName) {
        try {
            return mPackageManager.getApplicationInfo(packageName, 0).enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return false;
        }
    }


    /**
     * Install Exit Poll App
     * + CS Entry
     * + Dapil Uploader
     */
    private void installAllAppExitPoll(String[] appNameList) {

//        String[] appNameList = {"csentry.apk", "dapil_uploader.apk"};

        for (String appName : appNameList) {
            // check if system build more than api 24 (Nougat)
            // because for api 24 and above, we need FileProvider to access file
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "csentry/" + appName;
                Intent promptInstall = new Intent(Intent.ACTION_INSTALL_PACKAGE)
                        .setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider"
                                , new File(dirPath)),
                                "application/vnd.android.package-archive")
                        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                mContext.startActivity(promptInstall);

            } else {

                Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                        .setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/csentry/" + appName))
                                , "application/vnd.android.package-archive");
                mContext.startActivity(promptInstall);

            }


        } // endforeach
    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

}
