package com.ridhadanjanny.qcsmrc.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ridhadanjanny.qcsmrc.model.User;

/**
 * Initialize & Manage Shared Preference across app
 */
public class PreferenceManager {

    // get class name as string
    private String TAG = PreferenceManager.class.getSimpleName();

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // USER
    // Shared Preference
    SharedPreferences mUserPreference;
    // Editor for Shared Preference
    SharedPreferences.Editor mUserEditor;

    /**
     * Initialize & access the shared preference file
     * identified by unique filename from Config Class
     * @param context
     */
    public PreferenceManager(Context context) {

        // store application context to property,
        // so that it can accessible to whole method
        _context = context;

        // SHARED PREFERENCE : USER
        // create new shared preference to store user information
        mUserPreference = _context.getSharedPreferences(Config.SHARED_PREF_USER, PRIVATE_MODE);
        // write to a shared preference file
        mUserEditor = mUserPreference.edit();

    }

    /**
     * Add user information to shared preference
     * with Parcelable User Object
     * @param Parcelable user
     */
    public void addUser(User user) {
        mUserEditor.putString(Config.KEY_USER_EMAIL, user.getEmail());
        mUserEditor.putString(Config.KEY_USER_EDITED, user.getIsEdited());
        mUserEditor.putString(Config.KEY_USER_SUBMITTED, user.getIsSubmitted());
        mUserEditor.putString(Config.KEY_USER_SUBMITTED_PARTAI, user.getIsSubmittedPartai());
        mUserEditor.putString(Config.KEY_TIPE_RELAWAN, user.getTipeRelawan());
//        mUserEditor.putString(Config.KEY_USER_KODE_RELAWAN, user.getKodeRelawan());
        mUserEditor.putString(Config.KEY_USER_NAME, user.getNama());
        mUserEditor.putString(Config.KEY_USER_HP_1, user.getNoHP1());
        mUserEditor.putString(Config.KEY_USER_HP_2, user.getNoHP2());
        mUserEditor.putString(Config.KEY_USER_LONG_NUMBER, user.getLongNumber());
        mUserEditor.commit();

        Log.v(TAG, "User is stored in shared preference. " + user.getEmail() + ", " + user.getNama());
    }

    /**
     * Update user information to shared preference
     * with Parcelable User Object
     * @param Parcelable user
     */
    public void updateUser(User user) {
        mUserEditor.putString(Config.KEY_USER_EMAIL, user.getEmail());
        mUserEditor.putString(Config.KEY_USER_EDITED, user.getIsEdited());
        mUserEditor.putString(Config.KEY_USER_SUBMITTED, user.getIsSubmitted());
        mUserEditor.putString(Config.KEY_USER_SUBMITTED_PARTAI, user.getIsSubmittedPartai());
        mUserEditor.putString(Config.KEY_TIPE_RELAWAN, user.getTipeRelawan());
//        mUserEditor.putString(Config.KEY_USER_KODE_RELAWAN, user.getKodeRelawan());
        mUserEditor.putString(Config.KEY_USER_NAME, user.getNama());
        mUserEditor.putString(Config.KEY_USER_HP_1, user.getNoHP1());
        mUserEditor.putString(Config.KEY_USER_HP_2, user.getNoHP2());
        mUserEditor.putString(Config.KEY_USER_LONG_NUMBER, user.getLongNumber());
        mUserEditor.putString(Config.KEY_USER_PROV, user.getProv());
        mUserEditor.putString(Config.KEY_USER_DAPIL, user.getDapil());
        mUserEditor.putString(Config.KEY_USER_KABKOTA, user.getKab());
        mUserEditor.putString(Config.KEY_USER_ACCESS_FORM_CALEG, user.getIsEnableFormCaleg());
        mUserEditor.apply();

        Log.v(TAG, "User is updated in shared preferences. " + user.getEmail() + ", " + user.getNama() + ", " +
        user.getTipeRelawan() + ", " + user.getKodeRelawan() + ", " + user.getIsSubmitted() + ", " + user.getIsSubmittedPartai() +
        ", " + user.getProv() + ", " + user.getDapil() + ", " + user.getKab() + ", " + user.getIsEnableFormCaleg()
        );

    }

    /**
     * Remove user information to shared preference
     * @param Parcelable user
     */
    public void removeUser() {
        mUserEditor.remove(Config.KEY_USER_EMAIL);
        mUserEditor.remove(Config.KEY_TIPE_RELAWAN);
//        mUserEditor.remove(Config.KEY_USER_KODE_RELAWAN);
        mUserEditor.remove(Config.KEY_USER_NAME);
        mUserEditor.remove(Config.KEY_USER_HP_1);
        mUserEditor.remove(Config.KEY_USER_HP_2);
        mUserEditor.remove(Config.KEY_USER_LONG_NUMBER);
        mUserEditor.commit();

        Log.v(TAG, "User is delete in shared preferences.");
    }

    /**
     * Get user information to shared preference
     * @return Parcelable user
     */
    public User getUser() {
        User user = null;
        if(mUserPreference.getString(Config.KEY_USER_EMAIL, null) != null) {
            user = new User();
            user.setEmail(mUserPreference.getString(Config.KEY_USER_EMAIL, null));
            user.setIsEdited(mUserPreference.getString(Config.KEY_USER_EDITED, null));
            user.setIsSubmitted(mUserPreference.getString(Config.KEY_USER_SUBMITTED, null));
            user.setIsSubmittedPartai(mUserPreference.getString(Config.KEY_USER_SUBMITTED_PARTAI, null));
            user.setNama(mUserPreference.getString(Config.KEY_USER_NAME, null));
            user.setTipeRelawan(mUserPreference.getString(Config.KEY_TIPE_RELAWAN, null));
//            user.setKodeRelawan(mUserPreference.getString(Config.KEY_USER_KODE_RELAWAN, null));
            user.setNoHP1(mUserPreference.getString(Config.KEY_USER_HP_1, null));
            user.setNoHP2(mUserPreference.getString(Config.KEY_USER_HP_2, null));
            user.setLongNumber(mUserPreference.getString(Config.KEY_USER_LONG_NUMBER, null));
            user.setProv(mUserPreference.getString(Config.KEY_USER_PROV, null));
            user.setDapil(mUserPreference.getString(Config.KEY_USER_DAPIL, null));
            user.setKab(mUserPreference.getString(Config.KEY_USER_KABKOTA, null));
            user.setIsEnableFormCaleg(mUserPreference.getString(Config.KEY_USER_ACCESS_FORM_CALEG, null));
        }
        return user;
    }

}
