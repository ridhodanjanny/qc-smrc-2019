package com.ridhadanjanny.qcsmrc.app;

import android.content.Context;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.model.Caleg;
import com.ridhadanjanny.qcsmrc.model.JumlahSuaraEditor;
import com.ridhadanjanny.qcsmrc.model.KodeLokasi;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.model.realmObject.Vote;
import com.ridhadanjanny.qcsmrc.model.realmObject.VoteCaleg;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmManager {
    private static final String TAG = RealmManager.class.getSimpleName();

    // Hold result of Vote
    private RealmResults<Vote> realmVote;

    // Hold result of User / Relawan
    private RealmResults<User> realmUser;

    // Hold result of User / Relawan
    private RealmResults<Caleg> realmCaleg;

    public Context context;
    private MaterialDialog mDialog;

    public RealmManager(Context context) {
        this.context = context;
    }

    /**
     * Add Vote PARTAI
     * @param vote
     */
    public void addVote(final Vote vote) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insert(vote);
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "Data vote is inserted!");
                realm.close();
            }
        }
    }

    /**
     * Get data stored for president
     * @return ArrayList of Vote
     */
    public List<Vote> getVote(String voteType) {
        List<Vote> aVote = new ArrayList<Vote>();
        Realm realm = Realm.getDefaultInstance();
        realmVote = realm.where(Vote.class).equalTo("voteType", voteType).findAll();
        for (int i = 0; i < realmVote.size(); i++) {
            Vote vote = realmVote.get(i);
            aVote.add(vote);
        }

        Log.v(TAG, "Size vote : " + realmVote.size());

        return aVote;
    }


    /**
     * Add User
     * @param user
     */
    public void addUser(final User user) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insert(user);
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "Data user is inserted!");
                realm.close();
            }
        }
    }


    /**
     * Add User
     * @param
     */
    public void deleteUser(final String email, final String tipeRelawan) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final RealmResults<User> userResults = realm.where(User.class)
                                            .equalTo("email", email)
                                            .findAll();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    // delete all match
                    userResults.deleteAllFromRealm();
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "Data user is deleted!");
                realm.close();
            }
        }
    }

    /**
     * Get data stored for user
     * @param String email, tipeRelawan
     * @return ArrayList of User
     */
    public List<User> getUser(String email, String tipeRelawan) {
        List<User> aUser = new ArrayList<User>();
        Realm realm = Realm.getDefaultInstance();
        realmUser = realm.where(User.class)
                .equalTo("email", email)
                .findAll();

        for (int i = 0; i < realmUser.size(); i++) {
            User user = realmUser.get(i);
            aUser.add(user);
        }

        Log.v(TAG, "Size user : " + realmUser.size());

        return aUser;
    }


    /**
     * Add Caleg
     * @param user
     */
    public void addCaleg(final Caleg caleg) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insert(caleg);
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "Data caleg is inserted!");
                realm.close();
            }
        }
    }


    /**
     * Get partai by dapil
     * @param String
     * @return ArrayList<Caleg>
     */
    public List<Caleg> getPartaiByDapil(String dapil) {

        // set ArrayList for store Caleg
        List<Caleg> aPartai = new ArrayList<Caleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get result as ArrayList of Caleg by dapil param
        realmCaleg = realm.where(Caleg.class)
                .distinct("partai")
                .equalTo("dapil", dapil)
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmCaleg.size(); i++) {
            Caleg caleg = realmCaleg.get(i);
            aPartai.add(caleg);
        }

        Log.v(TAG, "Size partai : " + realmCaleg.size());

        // ArrayList of Partai
        return aPartai;
    }


    /**
     * Get caleg by partai
     * @param String
     * @return ArrayList<Caleg>
     */
    public ArrayList<Caleg> getCalegByPartai(String partai) {

        // set ArrayList for store Caleg
        ArrayList<Caleg> aCaleg = new ArrayList<Caleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get result as ArrayList of Caleg by dapil param
        RealmResults<Caleg> realmCaleg = realm.where(Caleg.class)
                .equalTo("partai", partai)
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmCaleg.size(); i++) {
            Caleg caleg = realmCaleg.get(i);
            aCaleg.add(caleg);
        }

        Log.v(TAG, "Size caleg : " + realmCaleg.size());

        // ArrayList of Caleg
        return aCaleg;
    }


    /**
     * Add Vote CALEG
     * @param voteCaleg
     */
    public void addVoteCaleg(final VoteCaleg voteCaleg) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.insert(voteCaleg);
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "Data vote caleg is inserted!");
                realm.close();
            }
        }
    }

    /**
     * UPDATE VOTE CALEG
     * @param voteCaleg
     */
    public void updateVoteCaleg(final String primaryID,
                                final String jumlahSuaraNew,
                                final String partai) {

        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<VoteCaleg> voteCaleg =
                            realm.where(VoteCaleg.class)
                                    .equalTo("namaPartai", partai) // get data by partai
                                    .and()
                                    .equalTo("voteCalegID", primaryID) // get data by primary ID
                                    .findAll();

                    if(voteCaleg != null) {
                        voteCaleg.setString("jumlahSuaraCaleg", jumlahSuaraNew); // change value of field
                    }
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "VoteCalegID " + primaryID + " change field jumlahSuaraCaleg to "
                        +  jumlahSuaraNew
                        + " VoteCaleg is updated!");
                realm.close();
            }
        }

    }

    /**
     * Get Vote Caleg (after user filled in the form) by partai
     * @param String
     * @return ArrayList<VoteCaleg>
     */
    public ArrayList<VoteCaleg> getVoteCalegByPartai(String partai) {

        // set ArrayList for store Caleg
        ArrayList<VoteCaleg> aVoteCaleg = new ArrayList<VoteCaleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get result as ArrayList of Caleg by dapil param
        RealmResults<VoteCaleg> realmVoteCaleg = realm.where(VoteCaleg.class)
                .equalTo("namaPartai", partai)
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmVoteCaleg.size(); i++) {
            VoteCaleg voteCaleg = realmVoteCaleg.get(i);
            aVoteCaleg.add(voteCaleg);
            Log.v(TAG, "Get data caleg : " + voteCaleg.getNamaCaleg() + " : " + voteCaleg.getJumlahSuaraCaleg());
        }

        // ArrayList of Caleg
        return aVoteCaleg;
    }

    /**
     * Get TOTAL SUARA SEMUA PARTAI Vote Caleg (after user filled in the form)
     * @param String
     * @return ArrayList<VoteCaleg>
     */

    public ArrayList<VoteCaleg> getAllVoteCaleg() {

        // set ArrayList for store Caleg
        ArrayList<VoteCaleg> aAllVoteCaleg = new ArrayList<VoteCaleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get result as ArrayList<VoteCaleg>
        String[] namaCalegField = {"TOTAL"};
        RealmResults<VoteCaleg> realmAllVoteCaleg = realm.where(VoteCaleg.class)
                .distinct("namaPartai")
                .in("namaCaleg", namaCalegField)
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmAllVoteCaleg.size(); i++) {
            VoteCaleg voteCaleg = realmAllVoteCaleg.get(i);
            aAllVoteCaleg.add(voteCaleg);
        }

        Log.v(TAG, "Size all vote caleg for TOTAL field : " + aAllVoteCaleg.size());

        // ArrayList of Caleg
        return aAllVoteCaleg;
    }


    /**
     * Get TOTAL SUARA TIDAK SAH & DPT Vote Caleg (after user filled in the form)
     * @param String
     * @return ArrayList<VoteCaleg>
     */

    public ArrayList<VoteCaleg> getSuaraTidakSahDPT() {
        // set ArrayList for store Caleg
        ArrayList<VoteCaleg> aDataSuaraTidakSahDPT = new ArrayList<VoteCaleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get result as ArrayList of V by dapil param
        // DISTINCT namaPartai where namaCaleg = "TOTAL"

        String[] namaCalegField = {"TOTAL"};
        RealmResults<VoteCaleg> realmDataSuaraTidakSahDPT = realm.where(VoteCaleg.class)
                .equalTo("namaPartai", "Suara Tidak Sah & DPT")
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmDataSuaraTidakSahDPT.size(); i++) {
            VoteCaleg voteSuaraTidakSahDPT = realmDataSuaraTidakSahDPT.get(i);
            aDataSuaraTidakSahDPT.add(voteSuaraTidakSahDPT);
        }

        Log.v(TAG, "Size all vote caleg for SUARA TIDAK SAH & DPT field : "
                + aDataSuaraTidakSahDPT.size());

        // ArrayList of Caleg
        return aDataSuaraTidakSahDPT;
    }


    /**
     * Delete VoteCaleg by partai
     * @param
     */
    public void deleteVoteCaleg(final String partai) {
        Realm realm = null;
        try {
            realm = Realm.getDefaultInstance();

            final RealmResults<VoteCaleg> voteCalegResults = realm.where(VoteCaleg.class)
                    .equalTo("namaPartai", partai)
                    .findAll();

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    // delete all match
                    voteCalegResults.deleteAllFromRealm();
                }
            });
        } finally {
            if(realm != null) {
                Log.e(TAG, "All VoteCaleg from " + partai + "is deleted!");
                realm.close();
            }
        }
    }


    /**
     * GET SELURUH FIELD DAN ROW VoteCaleg
     * @param String
     * @return ArrayList<VoteCaleg>
     */

    public ArrayList<VoteCaleg> getAllRowFieldVoteCaleg(final String email, final String tipeRelawan) {

        // set ArrayList for store Caleg
        ArrayList<VoteCaleg> aAllVoteCalegResult = new ArrayList<VoteCaleg>();
        // init realm object
        Realm realm = Realm.getDefaultInstance();

        // get all result as ArrayList<VoteCaleg>
        RealmResults<VoteCaleg> realmAllResult = realm.where(VoteCaleg.class)
                .equalTo("emailRelawan", email)
                .and()
                .equalTo("tipeRelawan", tipeRelawan)
                .findAll();

        // add each Caleg to ArrayList
        for (int i = 0; i < realmAllResult.size(); i++) {
            // get data per row
            VoteCaleg voteCalegRow = realmAllResult.get(i);
            // add row data to ArrayList<VoteCaleg>
            aAllVoteCalegResult.add(voteCalegRow);
        }

        Log.v(TAG, "Size all vote caleg for TOTAL field : " + aAllVoteCalegResult.size());

        // ArrayList of Caleg
        return aAllVoteCalegResult;
    }


//
//    /**
//     * Get data stored for partai
//     * @return ArrayList of Vote
//     */
//    public List<Vote> getVotePartai() {
//        List<Vote> aVote = new ArrayList<Vote>();
//        Realm realm = Realm.getDefaultInstance();
//        realmVote = realm.where(Vote.class).equalTo("voteType", "partai").findAll();
//        for (int i = 0; i < realmVote.size(); i++) {
//            Vote vote = realmVote.get(i);
//            aVote.add(vote);
//        }
//
//        Log.v(TAG, "Size vote : " + realmVote.size());
//
//        return aVote;
//
//    }




}
