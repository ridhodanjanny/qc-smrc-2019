package com.ridhadanjanny.qcsmrc.app;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.ridhadanjanny.qcsmrc.model.User;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = SmsBroadcastReceiver.class.getSimpleName();

    private User mUser;

    private final String serviceProviderNumber;
    private final String serviceProviderSmsCondition;

    private Listener listener;

    public SmsBroadcastReceiver(String serviceProviderNumber, String serviceProviderSmsCondition) {
        this.serviceProviderNumber = serviceProviderNumber;
        this.serviceProviderSmsCondition = serviceProviderSmsCondition;
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent.getAction().equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
            String smsSender = "";
            String smsBody = "";


                for(SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                    smsSender = smsMessage.getDisplayOriginatingAddress();
                    smsBody += smsMessage.getMessageBody();
                }

            if(smsSender.equals(serviceProviderNumber) && smsBody.startsWith(serviceProviderSmsCondition)) {

                Log.v(TAG, smsBody);

                if(listener != null) {
                    listener.onTextReceived(smsBody);
                }

                PendingIntent piSent = PendingIntent.getBroadcast(context, 32, new Intent(Config.SENT), 0);
                PendingIntent piDelivered = PendingIntent.getBroadcast(context, 33, new Intent(Config.DELIVERED), 0);

                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(Config.SHORT_NUMBER, null, "OK", null, piDelivered);

            }

        }

    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        public void onTextReceived(String text);
    }


}
