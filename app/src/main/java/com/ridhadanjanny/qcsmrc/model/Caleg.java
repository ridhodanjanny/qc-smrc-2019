package com.ridhadanjanny.qcsmrc.model;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class Caleg extends RealmObject implements Parcelable {

    // field
    String prov;
    String dapil;
    String partai;
    String noUrutCaleg;
    String nama;

    String tipeCaleg;
    String jumlahSuara;


    public Caleg() {
    }

    protected Caleg(Parcel in) {
        this.nama = in.readString();
        this.partai = in.readString();
        this.tipeCaleg = in.readString();
        this.jumlahSuara = in.readString();

        // field
        this.prov = in.readString();
        this.dapil = in.readString();
        this.noUrutCaleg = in.readString();
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getDapil() {
        return dapil;
    }

    public void setDapil(String dapil) {
        this.dapil = dapil;
    }

    public String getNoUrutCaleg() {
        return noUrutCaleg;
    }

    public void setNoUrutCaleg(String noUrutCaleg) {
        this.noUrutCaleg = noUrutCaleg;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPartai() {
        return partai;
    }

    public void setPartai(String partai) {
        this.partai = partai;
    }

    public String getTipeCaleg() {
        return tipeCaleg;
    }

    public void setTipeCaleg(String tipeCaleg) {
        this.tipeCaleg = tipeCaleg;
    }

    public String getJumlahSuara() {
        return jumlahSuara;
    }

    public void setJumlahSuara(String jumlahSuara) {
        this.jumlahSuara = jumlahSuara;
    }

    public static final Creator<Caleg> CREATOR = new Creator<Caleg>() {
        @Override
        public Caleg createFromParcel(Parcel in) {
            return new Caleg(in);
        }

        @Override
        public Caleg[] newArray(int size) {
            return new Caleg[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.nama);
        dest.writeString(this.partai);
        dest.writeString(this.tipeCaleg);
        dest.writeString(this.jumlahSuara);
        dest.writeString(this.dapil);
        dest.writeString(this.noUrutCaleg);
        dest.writeString(this.prov);
    }
}
