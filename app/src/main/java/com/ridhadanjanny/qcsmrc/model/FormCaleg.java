package com.ridhadanjanny.qcsmrc.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class FormCaleg implements Parcelable {

    String partai;
    ArrayList<Caleg> dataCaleg;

    public FormCaleg() {
    }

    public FormCaleg(String partai, ArrayList<Caleg> dataCaleg) {
        this.partai = partai;
        this.dataCaleg = dataCaleg;
    }

    public String getPartai() {
        return partai;
    }

    public void setPartai(String partai) {
        this.partai = partai;
    }

    public ArrayList<Caleg> getDataCaleg() {
        return dataCaleg;
    }

    public void setDataCaleg(ArrayList<Caleg> dataCaleg) {
        this.dataCaleg = dataCaleg;
    }

    protected FormCaleg(Parcel in) {
        partai = in.readString();
        dataCaleg = in.createTypedArrayList(Caleg.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(partai);
        dest.writeTypedList(dataCaleg);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FormCaleg> CREATOR = new Creator<FormCaleg>() {
        @Override
        public FormCaleg createFromParcel(Parcel in) {
            return new FormCaleg(in);
        }

        @Override
        public FormCaleg[] newArray(int size) {
            return new FormCaleg[size];
        }
    };

}
