package com.ridhadanjanny.qcsmrc.model;

public class JumlahSuaraEditor {
    String jumlahSuaraCurrent;
    String jumlahSuaraNew;

    public JumlahSuaraEditor() {

    }

    public String getJumlahSuaraCurrent() {
        return jumlahSuaraCurrent;
    }

    public void setJumlahSuaraCurrent(String jumlahSuaraCurrent) {
        this.jumlahSuaraCurrent = jumlahSuaraCurrent;
    }

    public String getJumlahSuaraNew() {
        return jumlahSuaraNew;
    }

    public void setJumlahSuaraNew(String jumlahSuaraNew) {
        this.jumlahSuaraNew = jumlahSuaraNew;
    }
}
