package com.ridhadanjanny.qcsmrc.model;

import android.os.Parcel;
import android.os.Parcelable;

public class KGParcelModel implements Parcelable {

    protected KGParcelModel(Parcel in) {
    }

    public static final Creator<KGParcelModel> CREATOR = new Creator<KGParcelModel>() {
        @Override
        public KGParcelModel createFromParcel(Parcel in) {
            return new KGParcelModel(in);
        }

        @Override
        public KGParcelModel[] newArray(int size) {
            return new KGParcelModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
