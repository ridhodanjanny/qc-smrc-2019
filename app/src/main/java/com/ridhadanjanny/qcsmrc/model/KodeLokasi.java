package com.ridhadanjanny.qcsmrc.model;

public class KodeLokasi {
    String kodeLokasiCurrent;
    String kodeLokasiNew;

    public String getKodeLokasiCurrent() {
        return kodeLokasiCurrent;
    }

    public void setKodeLokasiCurrent(String kodeLokasiCurrent) {
        this.kodeLokasiCurrent = kodeLokasiCurrent;
    }

    public String getKodeLokasiNew() {
        return kodeLokasiNew;
    }

    public void setKodeLokasiNew(String kodeLokasiNew) {
        this.kodeLokasiNew = kodeLokasiNew;
    }
}
