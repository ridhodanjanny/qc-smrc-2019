package com.ridhadanjanny.qcsmrc.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class User extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required // optional, but recommended.
    private String userID = UUID.randomUUID().toString();

    String isSubmitted;
    String isEdited;
    String isSubmittedPartai;
    String email;
    String tipeRelawan;
    String kodeRelawan;
    String nama;
    String noHP1;
    String noHP2;
    String longNumber;

    // wilayah
    String prov;
    String dapil;
    String kab;

    // kode lokasi tps
    String kodeLokasi;

    // access to form caleg c1
    String isEnableFormCaleg;

    public User() {
    }

    protected User(Parcel in) {
        this.isEdited = in.readString();
        this.isSubmitted = in.readString();
        this.isSubmittedPartai = in.readString();
//        this.isFill = in.readString();
        this.email = in.readString();
        this.tipeRelawan = in.readString();
        this.nama = in.readString();
        this.noHP1 = in.readString();
        this.noHP2 = in.readString();
        this.longNumber = in.readString();

        // add wilayah
        this.prov = in.readString();
        this.dapil = in.readString();
        this.kab = in.readString();

        // add kode lokasi tps
        this.kodeLokasi = in.readString();

        // add isEnableFormCaleg C1
        this.isEnableFormCaleg = in.readString();

    }

    public User(String email) {
        this.email = email;
    }

    public String getIsEnableFormCaleg() {
        return isEnableFormCaleg;
    }

    public void setIsEnableFormCaleg(String isEnableFormCaleg) {
        this.isEnableFormCaleg = isEnableFormCaleg;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getDapil() {
        return dapil;
    }

    public void setDapil(String dapil) {
        this.dapil = dapil;
    }

    public String getKab() {
        return kab;
    }

    public void setKab(String kab) {
        this.kab = kab;
    }

    public String getKodeLokasi() {
        return kodeLokasi;
    }

    public void setKodeLokasi(String kodeLokasi) {
        this.kodeLokasi = kodeLokasi;
    }

    public String getLongNumber() {
        return longNumber;
    }

    public void setLongNumber(String longNumber) {
        this.longNumber = longNumber;
    }

    public String getIsEdited() {
        return isEdited;
    }

    public void setIsEdited(String isEdited) {
        this.isEdited = isEdited;
    }

    public String getIsSubmittedPartai() {
        return isSubmittedPartai;
    }

    public void setIsSubmittedPartai(String isSubmittedPartai) {
        this.isSubmittedPartai = isSubmittedPartai;
    }

    public String getIsSubmitted() {
        return isSubmitted;
    }

    public void setIsSubmitted(String isSubmitted) {
        this.isSubmitted = isSubmitted;
    }

    public String getKodeRelawan() {
        return kodeRelawan;
    }

    public void setKodeRelawan(String kodeRelawan) {
        this.kodeRelawan = kodeRelawan;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTipeRelawan() {
        return tipeRelawan;
    }

    public void setTipeRelawan(String tipeRelawan) {
        this.tipeRelawan = tipeRelawan;
    }

    public String getNoHP1() {
        return noHP1;
    }

    public void setNoHP1(String noHP1) {
        this.noHP1 = noHP1;
    }

    public String getNoHP2() {
        return noHP2;
    }

    public void setNoHP2(String noHP2) {
        this.noHP2 = noHP2;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.tipeRelawan);
        dest.writeString(this.nama);
        dest.writeString(this.noHP1);
        dest.writeString(this.noHP2);
        dest.writeString(this.isSubmitted);
        dest.writeString(this.isEdited);
        dest.writeString(this.isSubmittedPartai);
        dest.writeString(this.longNumber);
        dest.writeString(this.isEnableFormCaleg);
    }


}
