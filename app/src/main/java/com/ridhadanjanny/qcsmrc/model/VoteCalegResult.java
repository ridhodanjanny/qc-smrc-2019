package com.ridhadanjanny.qcsmrc.model;

public class VoteCalegResult {

    String kodeLokasi;
    String dapil;
    String email;
    String namaPartai;
    String namaCaleg;
    String jumlahSuara;

    public VoteCalegResult() {
    }

    public String getKodeLokasi() {
        return kodeLokasi;
    }

    public void setKodeLokasi(String kodeLokasi) {
        this.kodeLokasi = kodeLokasi;
    }

    public String getDapil() {
        return dapil;
    }

    public void setDapil(String dapil) {
        this.dapil = dapil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNamaPartai() {
        return namaPartai;
    }

    public void setNamaPartai(String namaPartai) {
        this.namaPartai = namaPartai;
    }

    public String getNamaCaleg() {
        return namaCaleg;
    }

    public void setNamaCaleg(String namaCaleg) {
        this.namaCaleg = namaCaleg;
    }

    public String getJumlahSuara() {
        return jumlahSuara;
    }

    public void setJumlahSuara(String jumlahSuara) {
        this.jumlahSuara = jumlahSuara;
    }
}
