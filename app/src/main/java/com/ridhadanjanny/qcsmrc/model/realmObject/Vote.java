package com.ridhadanjanny.qcsmrc.model.realmObject;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

public class Vote extends RealmObject implements Parcelable {

    String email;
    String kodeTipeRelawan;
    String kodeRelawan;
    String namaRelawan;
    String noHp1;
    String noHp2;
    String voteType;
    String fieldName;
    String jumlahSuara;

    public Vote() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getKodeTipeRelawan() {
        return kodeTipeRelawan;
    }

    public void setKodeTipeRelawan(String kodeTipeRelawan) {
        this.kodeTipeRelawan = kodeTipeRelawan;
    }

    public String getKodeRelawan() {
        return kodeRelawan;
    }

    public void setKodeRelawan(String kodeRelawan) {
        this.kodeRelawan = kodeRelawan;
    }

    public String getNamaRelawan() {
        return namaRelawan;
    }

    public void setNamaRelawan(String namaRelawan) {
        this.namaRelawan = namaRelawan;
    }

    public String getNoHp1() {
        return noHp1;
    }

    public void setNoHp1(String noHp1) {
        this.noHp1 = noHp1;
    }

    public String getNoHp2() {
        return noHp2;
    }

    public void setNoHp2(String noHp2) {
        this.noHp2 = noHp2;
    }

    public String getVoteType() {
        return voteType;
    }

    public void setVoteType(String voteType) {
        this.voteType = voteType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getJumlahSuara() {
        return jumlahSuara;
    }

    public void setJumlahSuara(String jumlahSuara) {
        this.jumlahSuara = jumlahSuara;
    }

    protected Vote(Parcel in) {
        this.email = in.readString();
        this.kodeTipeRelawan = in.readString();
        this.kodeRelawan = in.readString();
        this.namaRelawan = in.readString();
        this.noHp1 = in.readString();
        this.noHp2 = in.readString();
        this.voteType = in.readString();
        this.fieldName = in.readString();
        this.jumlahSuara = in.readString();
    }

    public static final Creator<Vote> CREATOR = new Creator<Vote>() {
        @Override
        public Vote createFromParcel(Parcel in) {
            return new Vote(in);
        }

        @Override
        public Vote[] newArray(int size) {
            return new Vote[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.kodeTipeRelawan);
        dest.writeString(this.kodeRelawan);
        dest.writeString(this.namaRelawan);
        dest.writeString(this.noHp1);
        dest.writeString(this.noHp2);
        dest.writeString(this.voteType);
        dest.writeString(this.fieldName);
        dest.writeString(this.jumlahSuara);
    }
}
