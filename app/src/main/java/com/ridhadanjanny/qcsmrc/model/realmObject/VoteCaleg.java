package com.ridhadanjanny.qcsmrc.model.realmObject;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class VoteCaleg extends RealmObject implements Parcelable {

    @PrimaryKey
    @Required // optional, but recommended.
    private String voteCalegID = UUID.randomUUID().toString();

    String emailRelawan;
    String tipeRelawan;
    String namaRelawan;
    String dapil;
    String namaCaleg;
    String namaPartai;
    String jumlahSuaraCaleg;

    public VoteCaleg() {
    }

    protected VoteCaleg(Parcel in) {
        emailRelawan = in.readString();
        tipeRelawan = in.readString();
        namaRelawan = in.readString();
        dapil = in.readString();
        namaCaleg = in.readString();
        jumlahSuaraCaleg = in.readString();
        namaPartai = in.readString();

        this.voteCalegID = in.readString();
    }

    public String getVoteCalegID() {
        return voteCalegID;
    }

    public void setVoteCalegID(String voteCalegID) {
        this.voteCalegID = voteCalegID;
    }

    public String getNamaPartai() {
        return namaPartai;
    }

    public void setNamaPartai(String namaPartai) {
        this.namaPartai = namaPartai;
    }

    public static final Creator<VoteCaleg> CREATOR = new Creator<VoteCaleg>() {
        @Override
        public VoteCaleg createFromParcel(Parcel in) {
            return new VoteCaleg(in);
        }

        @Override
        public VoteCaleg[] newArray(int size) {
            return new VoteCaleg[size];
        }
    };

    public String getEmailRelawan() {
        return emailRelawan;
    }

    public void setEmailRelawan(String emailRelawan) {
        this.emailRelawan = emailRelawan;
    }

    public String getTipeRelawan() {
        return tipeRelawan;
    }

    public void setTipeRelawan(String tipeRelawan) {
        this.tipeRelawan = tipeRelawan;
    }

    public String getNamaRelawan() {
        return namaRelawan;
    }

    public void setNamaRelawan(String namaRelawan) {
        this.namaRelawan = namaRelawan;
    }

    public String getDapil() {
        return dapil;
    }

    public void setDapil(String dapil) {
        this.dapil = dapil;
    }

    public String getNamaCaleg() {
        return namaCaleg;
    }

    public void setNamaCaleg(String namaCaleg) {
        this.namaCaleg = namaCaleg;
    }

    public String getJumlahSuaraCaleg() {
        return jumlahSuaraCaleg;
    }

    public void setJumlahSuaraCaleg(String jumlahSuaraCaleg) {
        this.jumlahSuaraCaleg = jumlahSuaraCaleg;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emailRelawan);
        dest.writeString(tipeRelawan);
        dest.writeString(namaRelawan);
        dest.writeString(dapil);
        dest.writeString(namaCaleg);
        dest.writeString(jumlahSuaraCaleg);
        dest.writeString(namaPartai);
        dest.writeString(voteCalegID);

    }
}
