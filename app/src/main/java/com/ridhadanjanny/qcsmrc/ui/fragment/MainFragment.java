package com.ridhadanjanny.qcsmrc.ui.fragment;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.ExitPollListener;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.ui.fragment.qc.ListFormCalegFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.qc.PartaiFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.qc.PresidentFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.qc.VoteTypeFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.registrasi.InisiasiFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.settings.LongNumberFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.settings.SettingsFragment;

import java.util.ArrayList;
import java.util.List;

public class MainFragment extends BaseBackFragment {

    private static final int REQUEST_CODE_PICK_ACCOUNT = 23;
    private final String TAG = MainFragment.class.getSimpleName();

    // view
    private LinearLayout mRoot;
    private TextView mEmail;
    private LinearLayout mRegistrasi;
    private LinearLayout mQuickCount;

    private User mUser;

    private List<String> mPossibleEmails;

    private MultiplePermissionsListener allPermisionsListener;
    private PermissionRequestErrorListener errorListener;

    // button
    private LinearLayout mButtonDaftar;
    private LinearLayout mButtonSettings;
    private LinearLayout mButtonPresident;
    private LinearLayout mButtonPartai;
    private LinearLayout mButtonExitPoll;

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initView(view);
        return view;
    }

    /**
     * Initialize view
     *
     * @param view
     */
    private void initView(View view) {
        mRoot = view.findViewById(R.id.home_layout);
        mEmail = view.findViewById(R.id.home_email);
        mRegistrasi = view.findViewById(R.id.home_menu_1);
        mQuickCount = view.findViewById(R.id.home_menu_2);

        // main click button
        mButtonDaftar = view.findViewById(R.id.button_main_daftar);
        mButtonSettings = view.findViewById(R.id.button_main_settings);
        mButtonPresident = view.findViewById(R.id.button_main_president);
        mButtonPartai = view.findViewById(R.id.button_main_partai);
        mButtonExitPoll = view.findViewById(R.id.button_exit_poll);

        mUser = Application.getInstance().getPrefManager().getUser();

        createPermissionListener();

        Dexter.withActivity(_mActivity)
                .withPermissions(
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.GET_ACCOUNTS,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.READ_SMS,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(allPermisionsListener)
                .withErrorListener(errorListener)
                .check();
    }

    private void createPermissionListener() {
        MultiplePermissionsListener feedbackViewMultiplePermissionListener = new MultiplePermissionsListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    loadView();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                showPermissionRationale(token);
            }
        };

        allPermisionsListener = new CompositeMultiplePermissionsListener(feedbackViewMultiplePermissionListener,
                SnackbarOnAnyDeniedMultiplePermissionsListener
                        .Builder.with(mRoot, R.string.all_permissions_denied_feedback)
                        .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
                        .build());

        errorListener = new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.e("Dexter", "There was an error: " + error.toString());
            }
        };
    }

    private void loadView() {

        // DAFTAR BUTTON
        mButtonDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(InisiasiFragment.newInstance());
            }
        });

        // SETTINGS BUTTON
        mButtonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(SettingsFragment.newInstance());
            }
        });

        mButtonPresident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = Application.getInstance().getPrefManager().getUser();
                if (user == null ||
                        (user != null && user.getNama() == null && user.getTipeRelawan() == null)) {

                    showToast("Anda belum melakukan inisiasi. Silakan ke menu Daftar");

                } else {

                    if (user.getLongNumber() == null || user.getLongNumber().equals("")) {
                        showToast("Silakan mengisi long number terlebih dahulu!");
                        start(LongNumberFragment.newInstance());
                    } else {
                        start(PresidentFragment.newInstance());
                    }

                }

            }
        });

        mButtonPartai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = Application.getInstance().getPrefManager().getUser();
                if (user == null ||
                        (user != null && user.getNama() == null && user.getTipeRelawan() == null)) {
                    showToast("Anda belum melakukan inisiasi. Silakan ke menu Daftar");
                } else {

                    if (user.getLongNumber() == null || user.getLongNumber().equals("")) {
                        showToast("Silakan mengisi long number terlebih dahulu!");
                        start(LongNumberFragment.newInstance());
                    } else {
                        start(PartaiFragment.newInstance());
                    }

                }
            }
        });

        mButtonExitPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                User user = Application.getInstance().getPrefManager().getUser();

                if (user == null || (user.getNama() == null && user.getTipeRelawan() == null)) {
                    showToast("Anda belum melakukan inisiasi. Silakan ke menu Daftar");
                } else {
                    // packageName change to uploader
                    // different uploader around smrc and indi
                    ExitPollListener exitPollListener = new ExitPollListener(_mActivity,
                            "com.smrc.expol_april19"
                            , _mActivity.getPackageManager() );
                    // install app
                    exitPollListener.installExitPollApp();

                }

            }
        });


        // registrasi button clicked
        mRegistrasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(InisiasiFragment.newInstance());
            }
        });


        mQuickCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = Application.getInstance().getPrefManager().getUser();
                if (user == null ||
                        (user != null && user.getNama() == null && user.getTipeRelawan() == null)) {
                    Toast.makeText(_mActivity, "Anda belum melakukan inisiasi"
                            , Toast.LENGTH_SHORT).show();
                } else {
                    start(VoteTypeFragment.newInstance());
                }
            }
        });

        getEmail();

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void showPermissionRationale(final PermissionToken token) {
        new MaterialDialog.Builder(_mActivity)
                .title(R.string.permission_rationale_title)
                .content(R.string.permission_rationale_message)
                .positiveText(R.string.btn_ok)
                .negativeText(android.R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        token.cancelPermissionRequest();
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        token.continuePermissionRequest();
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        token.cancelPermissionRequest();
                    }
                })
                .show();
    }


    private void getEmail() {
        mUser = Application.getInstance().getPrefManager().getUser();
        if (mUser != null) {
            mEmail.setText(mUser.getEmail());
        } else {
            mPossibleEmails = new ArrayList<String>();
            Account[] accounts = AccountManager.get(_mActivity).getAccountsByType("com.google");
            for (Account account : accounts) {
                mPossibleEmails.add(account.name);
            }

            if (!mPossibleEmails.isEmpty() && mPossibleEmails.get(0) != null) {
                if (mPossibleEmails.size() > 0) {
                    showEmail();
                } else {
                    __setEmail(mPossibleEmails.get(0));
                }
            } else {
                getDeviceIMEI();
            }
        }
    }


    private void showToast(String msg) {
        Toast.makeText(_mActivity, msg, Toast.LENGTH_SHORT).show();
    }

    private void showLog(String mFact) {
        Log.v(TAG, mFact);
    }

    private void showEmail() {
        CharSequence[] charSequenceItems = mPossibleEmails.toArray(new CharSequence[mPossibleEmails.size()]);

        new MaterialDialog.Builder(_mActivity)
                .cancelable(false)
                .title(R.string.choose_email_title)
                .items(charSequenceItems)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (!(which < 0)) {
                            String email = mPossibleEmails.get(which);
                            __setEmail(email);
                            Log.e(TAG, "EMAIL ::" + email);
                        } else {
                            dialog.cancel();
                            Toast.makeText(_mActivity, R.string.required_email, Toast.LENGTH_SHORT).show();
                            showEmail();
                        }

                        return true;
                    }
                })
                .positiveText(R.string.btn_ok)
                .show();
    }

    private void __setEmail(String email) {
        mEmail.setText(email);
        mUser = new User(email);
        mUser.setIsEnableFormCaleg("0");
        Application.getInstance().getPrefManager().addUser(mUser);
    }

    private void getDeviceIMEI() {
        String deviceUniqueIdentifier = null;
        TelephonyManager tm = (TelephonyManager) _mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        if (null != tm) {
            if (ActivityCompat.checkSelfPermission(_mActivity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "READ_PHONE_STATE permission is already been granted");
                deviceUniqueIdentifier = tm.getDeviceId();
                if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length()) {
                    deviceUniqueIdentifier = Settings.Secure.getString(_mActivity.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
            }
        }

        if (null == deviceUniqueIdentifier || 0 == deviceUniqueIdentifier.length())
            Toast.makeText(_mActivity, "Info device tidak ditemukan!", Toast.LENGTH_SHORT).show();
        else {
            Log.e(TAG, deviceUniqueIdentifier);
            __setEmail(deviceUniqueIdentifier);
        }
    }


}
