package com.ridhadanjanny.qcsmrc.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ridhadanjanny.qcsmrc.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PrototypeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PrototypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrototypeFragment extends Fragment {


    public PrototypeFragment() {
        // Required empty public constructor
    }

    public static PrototypeFragment newInstance() {
        PrototypeFragment fragment = new PrototypeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_prototype, container, false);
    }


}
