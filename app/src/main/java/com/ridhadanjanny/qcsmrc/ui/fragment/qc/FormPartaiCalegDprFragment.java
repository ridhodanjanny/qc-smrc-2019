package com.ridhadanjanny.qcsmrc.ui.fragment.qc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyboardShortcutGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.ApiEndpoint;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.Caleg;
import com.ridhadanjanny.qcsmrc.model.FormCaleg;
import com.ridhadanjanny.qcsmrc.model.JumlahSuaraEditor;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.model.VoteCalegResult;
import com.ridhadanjanny.qcsmrc.model.realmObject.Vote;
import com.ridhadanjanny.qcsmrc.model.realmObject.VoteCaleg;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FormPartaiCalegDprFragment extends BaseBackFragment {

    private final String TAG = FormPartaiCalegDprFragment.class.getSimpleName();

    // dialog loader
    private MaterialDialog mDialog;

    // toolbar
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private User mUser;
    private RealmManager mRealmManager;

    // layout
    private LinearLayout mFormLayout;

    // button
    private LinearLayout mNext;
    private LinearLayout mBack;

    // partai
    private TextView mTitlePartai;

    private static final String ARG_FROM = "ARG_FROM";
    private static final String ARG_FORM_CALEG = "ARG_CALEG";

    private FormCaleg mFormCaleg;
    private ArrayList<FormCaleg> aDataFormCaleg;
    private int mFrom;

    // Data caleg : ArrayList<Caleg>
    private ArrayList<Caleg> aDataCaleg;

    private ArrayList<VoteCaleg> aDataCalegFilled;

    private ArrayList<String> aDataJumlahSuaraCalegFilled;
    private ArrayList<VoteCaleg> aDataSuaraTidakSahDPT;

    private ArrayList<VoteCaleg> aShowSuaraTidakSahDPT;
    private ArrayList<VoteCaleg> aDataRekapTotalPartai;

    // RESULT ALL ROW & FIELD OF VoteCaleg
    private ArrayList<VoteCaleg> aResultVoteCaleg;

    public static FormPartaiCalegDprFragment newInstance(int from, ArrayList<FormCaleg> aDataFormCaleg) {
        FormPartaiCalegDprFragment fragment = new FormPartaiCalegDprFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_FROM, from);
        args.putParcelableArrayList(ARG_FORM_CALEG, aDataFormCaleg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mFrom = getArguments().getInt(ARG_FROM);
            aDataFormCaleg = getArguments().getParcelableArrayList(ARG_FORM_CALEG);
        }

        // get ArrayList<FormCaleg> of this fragment
        mFormCaleg = aDataFormCaleg.get(mFrom);

        // get user info from SharedPreference
        mUser = Application.getInstance().getPrefManager().getUser();

        // init realm object
        mRealmManager = new RealmManager(_mActivity);

        // get ArrayList<Caleg> by partai, yg sudah terisi sebelumnya
        aDataCalegFilled = mRealmManager.getVoteCalegByPartai(mFormCaleg.getPartai());
        aDataJumlahSuaraCalegFilled = new ArrayList<String>(); // get ArrayList<String> jumlahSuaraCaleg
        if (aDataCalegFilled.size() > 0 || !aDataCalegFilled.isEmpty()) {
            for (VoteCaleg voteCaleg : aDataCalegFilled) {
//                showLog(voteCaleg.getNamaPartai() + ", " + voteCaleg.getDapil() + ", "
//                        + voteCaleg.getNamaCaleg() + " - " + voteCaleg.getJumlahSuaraCaleg());

                // add ArrayList<String> : jumlah suara caleg yg sudah terisi
                aDataJumlahSuaraCalegFilled.add(voteCaleg.getJumlahSuaraCaleg());
            }
        }

        // get ArrayList<VoteCaleg> by partai (Suara Tidak Sah & DPT)
        // result ArrayList<VoteCaleg>, add to showSummaryTextFormCaleg();
        aDataSuaraTidakSahDPT = mRealmManager.getSuaraTidakSahDPT();

        // get rekap total seluruh partai
        aDataRekapTotalPartai = mRealmManager.getAllVoteCaleg();

        // get ArrayList<Caleg>
        aDataCaleg = mFormCaleg.getDataCaleg();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_form_partai_caleg_dpr, container,
                false);
        initView(view);
        return view;

    }

    private void initView(View view) {

        // layout
        mFormLayout = view.findViewById(R.id.dpr_form_layout);

        // TOOLBAR
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        mTitlePartai = view.findViewById(R.id.caleg_partai);
        mNext = view.findViewById(R.id.form_c1_button_next);
        mBack = view.findViewById(R.id.form_c1_button_back);

        initToolbarNav(mToolbar);
        mToolbarTitle.setText("FORM C1 DPR RI");

        String partai = mFormCaleg.getPartai();
        mTitlePartai.setText(partai);

        /**
         * SHOW DATA TO EditText
         * Blank EditText OR Filled EditText
         */

        // show EditText based on condition
        if (mFrom < aDataFormCaleg.size() - 1) {
            showEditTextFormCaleg();
        } else {
            showSummaryTextFormCaleg();
        }

//        showToast("position : " + mFrom); // position 0
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show next fragment, if position of fragment less than size of FormCaleg size
                // REKAP SUARA FRAGMENT
                if (mFrom < aDataFormCaleg.size() - 1) {
                    // check if fragment position in the SUARA TIDAK SAH & DPT
                    if (mFrom == aDataFormCaleg.size() - 2) {

                        if (isValidSuaraTidakSahDPT() != "error") {
                            getFieldFromFormCaleg();
                            startWithPop(FormPartaiCalegDprFragment.newInstance(mFrom + 1, aDataFormCaleg));
                        }

                    } else {

                        if (isValid() != "error") {
                            getFieldFromFormCaleg();
                            startWithPop(FormPartaiCalegDprFragment.newInstance(mFrom + 1, aDataFormCaleg));
                        }

                    }

                } else {
                    new MaterialDialog.Builder(_mActivity)
                            .content(R.string.info_sumbit)
                            .cancelable(false)
                            .positiveText(R.string.btn_yes)
                            .negativeText(R.string.btn_no)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                    postData();
                                }
                            })
                            .show();
                }
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if fragment's position in 0 or greater than 0
                if ((mFrom - 1) >= 0) {
                    startWithPop(FormPartaiCalegDprFragment.newInstance(mFrom - 1, aDataFormCaleg));
                }
            }
        });

    }

    private void testPostData() {

//        for (VoteCaleg voteCaleg : aResultVoteCaleg) {
//            showLog(voteCaleg.getNamaPartai() + ", " + voteCaleg.getNamaCaleg() + ", " + voteCaleg.getJumlahSuaraCaleg());
//        }
//        showLog(String.valueOf(aResultVoteCaleg.size()));
    }

    private void postData() {


        // create dialog when process started
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        aResultVoteCaleg = mRealmManager.getAllRowFieldVoteCaleg(mUser.getEmail(), mUser.getTipeRelawan());
        List<User> aKodeLokasi = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());

        String kodeLokasi = "";
        for (int i = 0; i < aKodeLokasi.size(); i++) {
            if(i == 0) {
                kodeLokasi = aKodeLokasi.get(i).getKodeLokasi();
            }
        }

        ArrayList<VoteCalegResult> aVoteCalegResult = new ArrayList<VoteCalegResult>();
        for(VoteCaleg voteCaleg : aResultVoteCaleg) {
            VoteCalegResult voteCalegResult = new VoteCalegResult();
            voteCalegResult.setKodeLokasi(kodeLokasi);
            voteCalegResult.setDapil(mUser.getDapil());
            voteCalegResult.setEmail(mUser.getEmail());
            voteCalegResult.setNamaPartai(voteCaleg.getNamaPartai());
            voteCalegResult.setNamaCaleg(voteCaleg.getNamaCaleg());
            voteCalegResult.setJumlahSuara(voteCaleg.getJumlahSuaraCaleg());

            // add to ArrayList<VoteCalegResult>
            aVoteCalegResult.add(voteCalegResult);
        }

        String voteCalegJson = new Gson().toJson(aVoteCalegResult);

        AndroidNetworking.post(ApiEndpoint.POST_SUBMIT_VOTE_CALEG_RESULT)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .addBodyParameter("vote", voteCalegJson)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equalsIgnoreCase("ok")) {
                            startWithPop(MainFragment.newInstance());
                        } else {
                            showToast(response, 0);
                        }

                        mDialog.cancel();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, anError.getErrorDetail());
                    }
                });
    }

    private String isValidSuaraTidakSahDPT() {

        String error = "";

        // akumulasi total suara caleg seluruh partai
        int akumulasiTotalSuaraCaleg = 0;
        for (VoteCaleg voteCaleg : aDataRekapTotalPartai) {
            if (!voteCaleg.getNamaPartai().equals("Suara Tidak Sah & DPT")) {
                akumulasiTotalSuaraCaleg += Integer.parseInt(voteCaleg.getJumlahSuaraCaleg());
            }
        }

        int suaraTidakSah = 0;
        int DPT = 0;
        for (int i = 0; i < mFormLayout.getChildCount(); i++) {

            // get TextInputLayout
            TextInputLayout ITSuaraTidakSahDPT = (TextInputLayout) mFormLayout.getChildAt(i);
            // field name
            String namaField = ITSuaraTidakSahDPT.getHint().toString();
            // get jumlah suara from edittext
            String jumlahSuara = ITSuaraTidakSahDPT.getEditText().getText().toString();

            // check if form has empty
            if (jumlahSuara.isEmpty()) {
                Toast.makeText(_mActivity, namaField + " harus diisi", Toast.LENGTH_SHORT)
                        .show();
                error = "error";
                break;
            }

            if (i == 0) {
                suaraTidakSah = Integer.parseInt(jumlahSuara);
            } else if (i == 1) {
                DPT = Integer.parseInt(jumlahSuara);
            }
        }

        int akumulasiTotalSuaraCalegdanTidakSah = akumulasiTotalSuaraCaleg + suaraTidakSah;
        // cek akumulasi jumlah suara caleg & partai harus sama dengan kolom TOTAL
        if (DPT < akumulasiTotalSuaraCalegdanTidakSah) {
            showToast("Akumulasi total suara caleg & suara tidak sah: " + akumulasiTotalSuaraCalegdanTidakSah
                    + "\n" + "DPT : " + DPT + "\n\n" +
                    "DPT harus sama atau lebih besar dari akumulasi total suara caleg & suara tidak sah !", 1);
            error = "error";
        }

        return error;
    }


    /**
     * SHOW SUMMARY IN THE LAST OF ArrayList<FormCaleg>
     */
    private void showSummaryTextFormCaleg() {

        // create dialog when process started
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        // ArrayList<Caleg> aDataRekapSuaraCaleg = mFormCaleg.getDataCaleg();
        ArrayList<VoteCaleg> aDataRekapSuaraCaleg = mRealmManager.getAllVoteCaleg();

        for (VoteCaleg voteCaleg : aDataRekapSuaraCaleg) {
            showLog(voteCaleg.getNamaCaleg() + " : " + voteCaleg.getJumlahSuaraCaleg());
        }

        // summary of total of partai
        if (aDataRekapSuaraCaleg.size() > 0 || !aDataRekapSuaraCaleg.isEmpty()) {
            for (VoteCaleg rekapSuaraCaleg : aDataRekapSuaraCaleg) {
                if (!rekapSuaraCaleg.getNamaPartai().equals("Suara Tidak Sah & DPT")) {
                    addEditTextCaleg(rekapSuaraCaleg.getNamaPartai(),
                            rekapSuaraCaleg.getJumlahSuaraCaleg(), true);
                }
            }
            mDialog.cancel();
        }

        // summary of suara tidak sah & DPT
        if (aDataSuaraTidakSahDPT.size() > 0 || !aDataSuaraTidakSahDPT.isEmpty()) {
            for (VoteCaleg voteCaleg : aDataSuaraTidakSahDPT) {
                addEditTextCaleg(voteCaleg.getNamaCaleg(), voteCaleg.getJumlahSuaraCaleg(), true);
            }
            mDialog.cancel();
        }

    }

    /**
     * GET FIELD FROM FORM CALEG DPR RI C1
     *
     * @return void
     */
    private void getFieldFromFormCaleg() {

        // create dialog when process started
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        for (int i = 0; i < mFormLayout.getChildCount(); i++) {
            TextInputLayout ITPartai = (TextInputLayout) mFormLayout.getChildAt(i);
            // get input jumlah suara untuk disimpan di realm db
            String jumlahSuara = ITPartai.getEditText().getText().toString();
            String namaCaleg = ITPartai.getHint().toString();
            String namaCalegCleaned = cleanNamaCaleg(namaCaleg);
            // save to realm db!
            VoteCaleg voteCaleg = new VoteCaleg();
            voteCaleg.setEmailRelawan(mUser.getEmail());
            voteCaleg.setNamaCaleg(mUser.getNama());
            voteCaleg.setDapil(mUser.getDapil());
            voteCaleg.setTipeRelawan(mUser.getTipeRelawan());
            voteCaleg.setNamaCaleg(namaCalegCleaned);
            voteCaleg.setJumlahSuaraCaleg(jumlahSuara);
            voteCaleg.setNamaPartai(mFormCaleg.getPartai());

//            showLog(voteCaleg.getNamaCaleg() + " - " + voteCaleg.getJumlahSuaraCaleg());

            /**
             * PROBLEM : FIELD INPUT PARTAI DAN TOTAL : TIDAK BISA UPDATE DB !!!
             */

            // add VoteCaleg Form C1 DPR RI
            if (aDataCalegFilled.size() > 0 || !aDataCalegFilled.isEmpty()) {
                // update jumlahSuaraPartai current to new input
                mRealmManager.updateVoteCaleg(aDataCalegFilled.get(i).getVoteCalegID(),
                        voteCaleg.getJumlahSuaraCaleg(), mFormCaleg.getPartai());
            } else {
                mRealmManager.addVoteCaleg(voteCaleg);
            }
        }

        mDialog.cancel();
    }

    // form validation / if any empty form
    private String isValid() {

        String error = "";
        int akumulasiTotalSuaraCalegdanPartai = 0; // get akumulasi value seluruh field kecuali total
        int totalFieldValue = 0;
        for (int i = 0; i < mFormLayout.getChildCount(); i++) {
            // get TextInputLayout
            TextInputLayout ITPartai = (TextInputLayout) mFormLayout.getChildAt(i);
            // field name
            String namaField = ITPartai.getHint().toString();
            // get jumlah suara from edittext
            String jumlahSuara = ITPartai.getEditText().getText().toString();

            // check if form has empty
            if (jumlahSuara.isEmpty()) {
                Toast.makeText(_mActivity, namaField + " harus diisi", Toast.LENGTH_SHORT)
                        .show();
                error = "error";
                break;
            }

            // get value of akumulasi suara partai & caleg
            if (i < mFormLayout.getChildCount() - 1) {
                akumulasiTotalSuaraCalegdanPartai += Integer.parseInt(jumlahSuara);
            }

            // get value of total field
            if (i == mFormLayout.getChildCount() - 1) {
                totalFieldValue = Integer.parseInt(jumlahSuara);
            }
        }

        // cek akumulasi jumlah suara caleg & partai harus sama dengan kolom TOTAL
        if (akumulasiTotalSuaraCalegdanPartai != totalFieldValue) {
            showToast("Akumulasi suara partai & caleg : " + akumulasiTotalSuaraCalegdanPartai
                    + "\n" + "Total suara : " + totalFieldValue + "\n\n" +
                    "Akumulasi jumlah suara partai & caleg harus sama dengan total suara!", 1);
            error = "error";
        }

        return error;

    }


    private String cleanNamaCaleg(String namaCaleg) {
        String namaCalegUpdate = namaCaleg.replaceAll("\\d", "").substring(1);
        return namaCalegUpdate;
    }

    /**
     * SHOW EDIT TEXT ALL CALEG FORM BEFORE REKAP
     */
    private void showEditTextFormCaleg() {

        // create dialog when process started
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        // Caleg for each partai
        // get data jumlah suara berdasarkan partai, jika ada

        if (aDataCalegFilled.size() > 0 || !aDataCalegFilled.isEmpty()) {

            int count = 0;
            for (int i = 0; i < aDataCalegFilled.size(); i++) {

                if (i == 0 || i == aDataCalegFilled.size() - 1) {
                    addEditTextCaleg(aDataCalegFilled.get(i).getNamaCaleg(),
                            aDataCalegFilled.get(i).getJumlahSuaraCaleg(), false);
                } else {
                    addEditTextCaleg(count + " " + aDataCalegFilled.get(i).getNamaCaleg(),
                            aDataCalegFilled.get(i).getJumlahSuaraCaleg(), false);
                }

                count++;

            }

            mDialog.cancel();

        } else {

            // create a list of new blank form

            int count = 1;

            // add new field for PARTAI & TOTAL except last fragment
            if (mFrom < aDataFormCaleg.size() - 2) {
                addEditTextCaleg(" PARTAI", null, false);
            }

            for (Caleg caleg : aDataCaleg) {
                addEditTextCaleg(count + " " + caleg.getNama(), null, false);
                count++;
            }

            if (mFrom < aDataFormCaleg.size() - 2) {
                addEditTextCaleg(" TOTAL", null, false);
            }

            mDialog.cancel();

        }


    }

    private void showToast(String msg, int speed) {
        if (speed == 1) {
            Toast.makeText(_mActivity, msg, Toast.LENGTH_LONG)
                    .show();
        } else {
            Toast.makeText(_mActivity, msg, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * LOG : SHOW DATA CALEG BY CURRENT DAPIL, SORT BY PARTAI
     */
    private void showDataAsTest() {
        showLog("---------------------------");
        for (FormCaleg formCaleg : aDataFormCaleg) {
            showLog(formCaleg.getPartai()); // nama partai
            ArrayList<Caleg> aCaleg = formCaleg.getDataCaleg();
            for (Caleg caleg : aCaleg) {
                showLog(caleg.getPartai() + " - " + caleg.getNama()); // partai - nama caleg
            }
        }
    }

    private void addEditTextCaleg(String namaCaleg, String jumlahSuara, Boolean disableField) {
        EditText ETPartai = new EditText(_mActivity);
        ETPartai.setId(View.generateViewId());
        // set hint using nama caleg
        ETPartai.setHint(namaCaleg);

        // if jumlahSuara tidak ada, karena new form
        if (jumlahSuara != null) {
            ETPartai.setText(jumlahSuara);

            // if field EditText ingin di disable
            if (disableField) {
                ETPartai.setEnabled(false);
            }

        }

        // set input type as number
        ETPartai.setInputType(InputType.TYPE_CLASS_NUMBER);
        // Param for EditText
        LinearLayout.LayoutParams ETPartaiParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );
        ETPartai.setLayoutParams(ETPartaiParams);

        addTextInputLayout(ETPartai, ETPartaiParams);
    }

    private void addTextInputLayout(EditText ETPartai, LinearLayout.LayoutParams ETPartaiParams) {
        TextInputLayout TILayout = new TextInputLayout(_mActivity);
        TILayout.setId(View.generateViewId());
        LinearLayout.LayoutParams TILayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TILayout.setLayoutParams(TILayoutParams);
        TILayout.addView(ETPartai, ETPartaiParams);

        // add new TextInputLayout
        mFormLayout.addView(TILayout, TILayoutParams);
    }


    @Override
    public boolean onBackPressedSupport() {
        new MaterialDialog.Builder(_mActivity)
                .content(R.string.info_cancel)
                .cancelable(false)
                .positiveText(R.string.btn_yes)
                .negativeText(R.string.btn_no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.cancel();
                        startWithPop(MainFragment.newInstance());
                    }
                })
                .show();
        return true;
    }


    private void showLog(String msg) {
        Log.d(TAG, msg);
    }


}
