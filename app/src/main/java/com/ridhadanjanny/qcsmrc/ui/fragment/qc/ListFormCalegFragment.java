package com.ridhadanjanny.qcsmrc.ui.fragment.qc;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.Caleg;
import com.ridhadanjanny.qcsmrc.model.FormCaleg;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.model.realmObject.Vote;
import com.ridhadanjanny.qcsmrc.model.realmObject.VoteCaleg;

import java.util.ArrayList;
import java.util.List;

public class ListFormCalegFragment extends BaseBackFragment {

    private final String TAG = ListFormCalegFragment.class.getSimpleName();

    // toolbar
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private TextView mFormNotice;

    private User mUser;
    private RealmManager mRealmManager;

    private ArrayList<FormCaleg> aDataFormCaleg;
    private ArrayList<Caleg> aCalegByPartai;

    // get partai
    private List<Caleg> aDataPartai;

    private LinearLayout mFormButton;

    public static ListFormCalegFragment newInstance() {
        ListFormCalegFragment fragment = new ListFormCalegFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get current user from SharedPreference
        mUser = Application.getInstance().getPrefManager().getUser();
        // init realmmanager
        mRealmManager = new RealmManager(_mActivity);

        // get all data partai by dapil
        aDataPartai = mRealmManager.getPartaiByDapil(mUser.getDapil());

        /**
         * Create ArrayList<FormCaleg>
         * FormCaleg digunakan untuk menampilkan form caleg
         */
        aDataFormCaleg = new ArrayList<FormCaleg>();
        for (Caleg caleg : aDataPartai) {

            // get caleg by partai, ArrayList<Caleg>
            aCalegByPartai = mRealmManager.getCalegByPartai(caleg.getPartai());

            // create formCaleg for each partai and Caleg object within,
            // then add to ArrayList<FormCaleg>
            FormCaleg formCaleg = new FormCaleg(caleg.getPartai(), aCalegByPartai);
            aDataFormCaleg.add(formCaleg);

        }

        // new fragment : Add Surat Suara Tidak Sah & DPT
        ArrayList<Caleg> aCalegByPartaiTambahan = new ArrayList<Caleg>();
        String[] fieldNameTambahan = {"Suara Tidak Sah", "DPT"};
        for (int i = 0; i < fieldNameTambahan.length; i++) {
            Caleg calegForFieldNameTambahan = new Caleg();
            calegForFieldNameTambahan.setNama(fieldNameTambahan[i]);
            // add nama caleg to ArrayList<Caleg>
            aCalegByPartaiTambahan.add(calegForFieldNameTambahan);
        }

        // FormCaleg for Suara Tidak Sah & DPT
        FormCaleg formCalegFieldTambahan = new FormCaleg("Suara Tidak Sah & DPT", aCalegByPartaiTambahan);

        // LAST FRAGMENT : Add summary of filled form
        // get data [TOTAL] from VoteCaleg
        ArrayList<VoteCaleg> aAllVoteCaleg = mRealmManager.getAllVoteCaleg();
        // rekap partai & total suara
        ArrayList<Caleg> aRekapTotalVoteCaleg = new ArrayList<Caleg>();
        if(aAllVoteCaleg.size() > 0) {
            for(VoteCaleg voteCaleg : aAllVoteCaleg) {
                Caleg calegForGetTotalValue = new Caleg();
                calegForGetTotalValue.setNama(voteCaleg.getNamaPartai());
                calegForGetTotalValue.setJumlahSuara(voteCaleg.getJumlahSuaraCaleg());

                // add nama caleg to ArrayList<Caleg>
                aRekapTotalVoteCaleg.add(calegForGetTotalValue);
            }
        }

        // FormCaleg for Suara Tidak Sah & DPT
        FormCaleg formCalegRekapSuara = new FormCaleg("REKAPITULASI TOTAL SUARA & DPT", aRekapTotalVoteCaleg);

        // add FormCaleg of 'SUARA TIDAK SAH & DPT' to ArrayList<FormCaleg> as fragment later
        aDataFormCaleg.add(formCalegFieldTambahan);
        // add FormCaleg of 'REKAP SUARA & DPT' to ArrayList<FormCaleg> as fragment later
        aDataFormCaleg.add(formCalegRekapSuara);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_list_form_caleg, container, false);

        initView(view);

        return view;

    }

    private void initView(View view) {

        // TOOLBAR
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // notice
        mFormNotice = view.findViewById(R.id.caleg_form_notice);

        initToolbarNav(mToolbar);
        mToolbarTitle.setText("FORM C1 DPR RI");

        // CREATE LINEARLAYOUT WITHIN LAYOUT FORM

        mFormButton = view.findViewById(R.id.form_c1_button);
        mFormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(FormPartaiCalegDprFragment.newInstance(0, aDataFormCaleg));
            }
        });

        // hidden button if user is not from 19 dapil registered
        if(aDataPartai.size() == 0 || aDataPartai.isEmpty()) {
            mFormButton.setVisibility(View.INVISIBLE);
            mFormNotice.setVisibility(View.VISIBLE);
        }

    }

    /**
     * LOG : SHOW DATA CALEG BY CURRENT DAPIL, SORT BY PARTAI
     */
    private void showDataAsTest() {
        for (FormCaleg formCaleg : aDataFormCaleg) {
            showLog(formCaleg.getPartai()); // nama partai
            ArrayList<Caleg> aCaleg = formCaleg.getDataCaleg();
            for (Caleg caleg : aCaleg) {
                showLog(caleg.getPartai() + " - " + caleg.getNama()); // partai - nama caleg
            }
        }
    }

    private void showLog(String msg) {
        Log.d(TAG, msg);
    }

}
