package com.ridhadanjanny.qcsmrc.ui.fragment.qc;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.Config;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.model.realmObject.Vote;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

public class PartaiFragment extends BaseBackFragment {

    private static final String TAG = PartaiFragment.class.getSimpleName();
    private LinearLayout mFormLayout;

    // sim card list
//    private final ArrayList<Integer> simCardList = new ArrayList<Integer>();
//    private final ArrayList<String> simCardNumber = new ArrayList<String>();

    // toolbar
    private TextView mToolbarTitle;
    private Toolbar mToolbar;

    // tipe relawan & kode lokasi
    private TextView mTipeRelawan;
    private ArrayAdapter<String> mAdapterKodeLokasi;
    private MaterialSpinner mKodeLokasi;

    // submit button
    private LinearLayout mSubmit;

    // jumlah suara
    private ArrayList<String> aDataJumlahSuara;

    // user
    private User mUser;

    // realm manager
    private RealmManager mRealmManager;

    // kode lokasi current
    private ArrayList<String> aDataKodeLokasiCurrent;

    // kode tps below toolbar
//    private TextView mKodeTps;
//    private LinearLayout mKodeTpsLayout;

    public static PartaiFragment newInstance() {
        PartaiFragment fragment = new PartaiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;

        // get user data
        mUser = Application.getInstance().getPrefManager().getUser();

        // init realm manager
        mRealmManager = new RealmManager(_mActivity);

        // get data of kode lokasi from realm db
        List<User> users = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());
        aDataKodeLokasiCurrent = new ArrayList<String>();
        for (User user : users) {
            aDataKodeLokasiCurrent.add(user.getKodeLokasi());
        }

        // editable form
        view = inflater.inflate(R.layout.fragment_partai, container, false);
        initView(view);

        // uneditable form
        // if user belum submit, tampilkan layout blank form
//        if(mUser.getIsSubmittedPartai() == null) {
//            view = inflater.inflate(R.layout.fragment_partai, container, false);
//            initView(view);
//        } else {
//            view = inflater.inflate(R.layout.fragment_partai_filled, container, false);
//            initViewFilled(view);
//        }

        return view;

    }


    private void initView(View view) {
        // form layout container
        mFormLayout = view.findViewById(R.id.dpr_form_layout);

        // inflate toolbar
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // tipe relawan
        mTipeRelawan = view.findViewById(R.id.presiden_tipe_relawan);
        // kode lokasi spinner
        mKodeLokasi = view.findViewById(R.id.presiden_kode_lokasi);

        // submit
        mSubmit = view.findViewById(R.id.submit_layout);

        initToolbarNav(mToolbar);
        mToolbarTitle.setText(R.string.partai_toolbar_title);

        showKodeAndTipeRelawanAtTop();

        // load data partai
        ArrayList<String> listPartai = Config.getPartai();

        // create InputTextLayout & EditText for each partai
        for (String partai : listPartai) {
            addEditTextCaleg(partai, null);
        }

        // submit button clicked
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                aDataJumlahSuara = new ArrayList<String>();

                // if form validation has error / has empty form
                if (isValid() != "error") {

                    // show dialog for validation
                    new MaterialDialog.Builder(_mActivity)
                            .content(R.string.info_upload)
                            .cancelable(false)
                            .positiveText(R.string.btn_yes)
                            .negativeText(R.string.btn_no)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                    for (int i = 0; i < mFormLayout.getChildCount(); i++) {
                                        // get TextInputLayout
                                        TextInputLayout ITPartai = (TextInputLayout) mFormLayout.getChildAt(i);
                                        // field name
                                        String namaField = ITPartai.getHint().toString();
                                        // get jumlah suara from edittext
                                        String jumlahSuara = ITPartai.getEditText().getText().toString();

                                        // add vote data / jumlah suara tiap partai ke ArrayList
                                        aDataJumlahSuara.add(jumlahSuara);
                                        // store data vote to realm db / local db
                                        storeToRealmDb(namaField, jumlahSuara);
                                    }

                                    int totalJumlahSuara = 0;
                                    int totalDpt = 0;

                                    // akumulasi seluruh suara partai
                                    for (int i = 0; i < aDataJumlahSuara.size(); i++) {
                                        if (i < 17) {
                                            totalJumlahSuara += Integer.parseInt(aDataJumlahSuara.get(i));
                                        }

                                        // get value of DPT
                                        if (i == 17) {
                                            totalDpt += Integer.parseInt(aDataJumlahSuara.get(i));
                                        }
                                    }

                                    showLog(String.valueOf(totalJumlahSuara));
                                    showLog(String.valueOf(totalDpt));

                                    // cek Jumlah total suara presiden tidak boleh lebih besar daripada DPT
                                    if (totalDpt >= totalJumlahSuara) {
                                        // send sms message
                                        sendSmsMessage(aDataJumlahSuara, mUser);

                                        // user has submitted president form and
                                        // save to sharedPreference
                                        mUser.setIsSubmittedPartai("1");
                                        Application.getInstance().getPrefManager().updateUser(mUser);

                                        // back to vote type
                                        startWithPop(MainFragment.newInstance());
                                    } else {
                                        showLongToast("Total Jumlah Suara : " + totalJumlahSuara +
                                                "\nDPT : " + totalDpt + "\n" +
                                                "Jumlah total suara partai tidak boleh lebih besar daripada DPT !");
                                    }

                                    // clear jumlah data vote presiden
                                    aDataJumlahSuara.clear();

                                } // end of dialog click
                            })
                            .show();
                }

            }
        });

    }

    private void showLongToast(String message) {
        Toast.makeText(_mActivity,
                message, Toast.LENGTH_LONG).show();
    }

    private void showLog(String message) {
        Log.d(TAG, message);
    }

    private void showKodeAndTipeRelawanAtTop() {

        // SET TIPE RELAWAN BELOW TOOLBAR
        if (mUser.getTipeRelawan() != null) {
            mTipeRelawan.setText(mUser.getTipeRelawan());
        }

        // SET ADAPTER FOR KODE LOKASI TPS
        mAdapterKodeLokasi = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataKodeLokasiCurrent
        );

        mAdapterKodeLokasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mKodeLokasi.setAdapter(mAdapterKodeLokasi);

        // set default kode lokasi tps
        String kodeLokasiDefault = aDataKodeLokasiCurrent.get(0);
        mKodeLokasi.setSelection(aDataKodeLokasiCurrent.indexOf(kodeLokasiDefault) + 1);

        mKodeLokasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void initViewFilled(View view) {

        // form layout container
        mFormLayout = view.findViewById(R.id.dpr_form_layout);

        // inflate toolbar
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // submit
        mSubmit = view.findViewById(R.id.submit_layout);

        initToolbarNav(mToolbar);
        mToolbarTitle.setText(R.string.partai_toolbar_title);

        // load data partai
        ArrayList<String> listPartai = Config.getPartai();

        // init realm manager
        String voteType = "Partai";
        List<Vote> votes = mRealmManager.getVote(voteType);

        // create InputTextLayout & EditText for each partai

        for (int i = 0; i < votes.size(); i++) {
            String namaPartai = votes.get(i).getFieldName();
            String jumlahSuara = votes.get(i).getJumlahSuara();
            addEditTextCaleg(namaPartai, jumlahSuara);
        }
    }


    private void storeToRealmDb(String namaField, String jumlahSuara) {
        Vote vote = new Vote();
        vote.setEmail(mUser.getEmail());
        vote.setKodeTipeRelawan(mUser.getTipeRelawan());
//        vote.setKodeRelawan(mUser.getKodeRelawan());
        vote.setNamaRelawan(mUser.getNama());
        vote.setNoHp1(mUser.getNoHP1());
        vote.setNoHp2(mUser.getNoHP2());
        vote.setVoteType("Partai");
        vote.setFieldName(namaField);
        vote.setJumlahSuara(jumlahSuara);

        // add vote data to realm db
        mRealmManager.addVote(vote);
    }

    // form validation / if any empty form
    private String isValid() {

        mUser = Application.getInstance().getPrefManager().getUser();

        String error = "";
        for (int i = 0; i < mFormLayout.getChildCount(); i++) {
            // get TextInputLayout
            TextInputLayout ITPartai = (TextInputLayout) mFormLayout.getChildAt(i);
            // field name
            String namaField = ITPartai.getHint().toString();
            // get jumlah suara from edittext
            String jumlahSuara = ITPartai.getEditText().getText().toString();

            // check if form has empty
            if (jumlahSuara.isEmpty()) {
                Toast.makeText(_mActivity, namaField + " harus diisi", Toast.LENGTH_SHORT)
                        .show();
                error = "error";
                break;
            }

            // cek long number sudah diisi
            if (mUser.getLongNumber() == null || mUser.getLongNumber().isEmpty()) {
                showToast("Long number harus diisi!\nCek menu settings - Long Number");
                error = "error";
                break;
            }

            // cek opsi kode lokasi sudah dipilih
            if(mKodeLokasi.getSelectedItem() == null) {
                showToast("Kode lokasi harus dipilih !");
                error = "error";
                break;
            }

        }

        return error;
    }

    private void showToast(String msg) {
        Toast.makeText(_mActivity,
                msg, Toast.LENGTH_SHORT).show();
    }


    private void sendSmsMessage(ArrayList<String> aDataJumlahSuara, User user) {

        mUser = Application.getInstance().getPrefManager().getUser();

        // get kode sms tipe relawan
        HashMap<String, String> mapKodeSmsTipeRelawan = Config.getKodeSmsTipeRelawan();
        String kodeSmsTipeRelawan = mapKodeSmsTipeRelawan.get(user.getTipeRelawan());

        // get kode lokasi TPS
        String kodeLokasiTPS = mKodeLokasi.getSelectedItem().toString();

        String longNumber = null;
        if (mUser.getLongNumber() != null) {
            longNumber = mUser.getLongNumber();
        }

        String[] destinationAddress = {
                Config.SHORT_NUMBER,
                longNumber
        };

        // choose 1st sim card active
        // chooseSimCard();

        String scAddress = null;
//        PendingIntent sentIntent = null, deliveryIntent = null;

        PendingIntent piSent = PendingIntent.getBroadcast(_mActivity, 30, new Intent(Config.SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(_mActivity, 31, new Intent(Config.DELIVERED), 0);

        String kodeTipeRelawan = user.getTipeRelawan();
//        String kodeRelawan = user.getKodeRelawan();

        String smsPartaiDpr = Config.ENTITY + " " + kodeSmsTipeRelawan + Config.QC_PARTAI + "#" + kodeLokasiTPS;
        for (String jumlahSuara : aDataJumlahSuara) {
            smsPartaiDpr += "#" + jumlahSuara;
        }

        // send SMS
        for (int i = 0; i < destinationAddress.length; i++) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(destinationAddress[i], scAddress, smsPartaiDpr, piSent, piDelivered);
        }

        Toast.makeText(_mActivity, "Input data voting partai berhasil !", Toast.LENGTH_LONG)
                .show();

    }

//    @TargetApi(Build.VERSION_CODES.M)
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
//    private void chooseSimCard() {
//        SubscriptionManager subscriptionManager;
//        subscriptionManager = (SubscriptionManager) _mActivity.getSystemService(SubscriptionManager.class);
//        if (ActivityCompat.checkSelfPermission(_mActivity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//
//        final List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
//        for (SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
//            int subscriptionId = subscriptionInfo.getSubscriptionId();
//            simCardList.add(subscriptionId);
//            simCardNumber.add(subscriptionInfo.getNumber());
//        }
//    }


    private void addEditTextCaleg(String partai, String jumlahSuara) {
        EditText ETPartai = new EditText(_mActivity);
        ETPartai.setId(View.generateViewId());
        // set hint using nama caleg
        ETPartai.setHint(partai);
        // set input type as number
        ETPartai.setInputType(InputType.TYPE_CLASS_NUMBER);
        // Param for EditText
        LinearLayout.LayoutParams ETPartaiParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );
        ETPartai.setLayoutParams(ETPartaiParams);

        if (jumlahSuara != null) {
            ETPartai.setText(jumlahSuara);
            ETPartai.setEnabled(false);
        }

        addTextInputLayout(ETPartai, ETPartaiParams);
    }

    private void addTextInputLayout(EditText ETPartai, LinearLayout.LayoutParams ETPartaiParams) {
        TextInputLayout TILayout = new TextInputLayout(_mActivity);
        TILayout.setId(View.generateViewId());
        LinearLayout.LayoutParams TILayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TILayout.setLayoutParams(TILayoutParams);
        TILayout.addView(ETPartai, ETPartaiParams);

        // add new TextInputLayout
        mFormLayout.addView(TILayout, TILayoutParams);
    }
}
