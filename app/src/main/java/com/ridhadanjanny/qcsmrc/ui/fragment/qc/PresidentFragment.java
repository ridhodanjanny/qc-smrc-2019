package com.ridhadanjanny.qcsmrc.ui.fragment.qc;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.Config;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.Caleg;
import com.ridhadanjanny.qcsmrc.model.KodeLokasi;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.model.realmObject.Vote;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

public class PresidentFragment extends BaseBackFragment {

    private final String TAG = PresidentFragment.class.getSimpleName();

    private final ArrayList<Integer> simCardList = new ArrayList<Integer>();
    private final ArrayList<String> simCardNumber = new ArrayList<String>();

    private ArrayList<Integer> aAkumulasiJumlahSuara = new ArrayList<Integer>();
    private Integer jumlahDpt;

    private User mUser;
    private Caleg mPresiden;

    // toolbar
    private TextView mToolbarTitle;
    private Toolbar mToolbar;

    // kode tps below toolbar
    private TextView mKodeTps;
    private LinearLayout mKodeTpsLayout;

    // form container
    private LinearLayout mFormLayout;

    // form input
    private TextView mTipeRelawan;
    private ArrayAdapter<String> mAdapterKodeLokasi;
    private MaterialSpinner mKodeLokasi;
    private EditText mPresiden1;
    private EditText mPresiden2;
    private EditText mPresidenTidakSah;
    private EditText mPresidenDpt;
    private CardView mSubmit;

    // arraylist of vote
    private ArrayList<String> aDataVote = new ArrayList<String>();

    // arraylist of Vote Class for realm db
    private ArrayList<Vote> aDataVoteRealmDb;

    // dialog
    private MaterialDialog mDialog;

    // realm manager
    private RealmManager mRealmManager;

    // kode lokasi current
    private ArrayList<String> aDataKodeLokasiCurrent;

    public static PresidentFragment newInstance() {
        PresidentFragment fragment = new PresidentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;

        // get current user
        mUser = Application.getInstance().getPrefManager().getUser();

        // init realm manager
        mRealmManager = new RealmManager(_mActivity);

        // get data of kode lokasi from realm db
        List<User> users = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());
        aDataKodeLokasiCurrent = new ArrayList<String>();
        for (User user : users) {
            aDataKodeLokasiCurrent.add(user.getKodeLokasi());
        }


        // editable form
        view = inflater.inflate(R.layout.fragment_president, container, false);
        initView(view);

        // uneditable form
//         if user had submitted, show filled form
//        if (mUser.getIsSubmitted() == null) {
//            view = inflater.inflate(R.layout.fragment_president, container, false);
//            initView(view);
//        } else {
//            view = inflater.inflate(R.layout.fragment_president_filled, container, false);
//            initViewFilled(view);
//        }

        return view;
    }


    private void initView(View view) {
        // inflate toolbar
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // form container
        mFormLayout = view.findViewById(R.id.presiden_form_layout);

        // inflate edittext
        mTipeRelawan = view.findViewById(R.id.presiden_tipe_relawan);
        mKodeLokasi = view.findViewById(R.id.presiden_kode_lokasi);
        mPresiden1 = view.findViewById(R.id.presiden_1);
        mPresiden2 = view.findViewById(R.id.presiden_2);
        mPresidenTidakSah = view.findViewById(R.id.presiden_tidak_sah);
        mPresidenDpt = view.findViewById(R.id.presiden_dpt);
        mSubmit = view.findViewById(R.id.presiden_submit);

        // assign kode tps below toolbar
        mKodeTpsLayout = view.findViewById(R.id.presiden_kode_tps_layout);
        mKodeTps = view.findViewById(R.id.presiden_kode_tps);

        // add back nav to toolbar
        initToolbarNav(mToolbar);
        mToolbarTitle.setText(R.string.presiden_toolbar_title);

        showKodeAndTipeRelawanAtTop();

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // if form input not empty
                if (isValid()) {

                    // initiate arraylist for jumlah suara
                    //aDataVote = new ArrayList<String>();

                    // show dialog for validation
                    new MaterialDialog.Builder(_mActivity)
                            .content(R.string.info_upload)
                            .cancelable(false)
                            .positiveText(R.string.btn_yes)
                            .negativeText(R.string.btn_no)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.cancel();
                                    // loop edittext for getting jumlah suara in each edittext within textinputlayout
                                    for (int i = 1; i < mFormLayout.getChildCount(); i++) {
                                        if (i < mFormLayout.getChildCount() - 1) {

                                            // get TextInputLayout
                                            TextInputLayout ITPresiden = (TextInputLayout) mFormLayout.getChildAt(i);
                                            // get nama presiden from hint
                                            String namaField = ITPresiden.getHint().toString();
                                            // get jumlah suara from edittext
                                            String jumlahSuara = ITPresiden.getEditText().getText().toString();

                                            // add vote data to arraylist
                                            // to use as sms content to sms gateway
                                            aDataVote.add(jumlahSuara);

                                            // store to realm db / local db
                                            storeToRealmDb(namaField, jumlahSuara);

                                        }

                                    }

                                    int totalJumlahSuara = 0;
                                    int totalDpt = 0;
                                    for (int i = 0; i < aDataVote.size(); i++) {
                                        if (i < 3) {
                                            totalJumlahSuara += Integer.parseInt(aDataVote.get(i));
                                        }

                                        if (i == 3) {
                                            totalDpt += Integer.parseInt(aDataVote.get(i));
                                        }
                                    }

                                    showLog(String.valueOf(totalJumlahSuara));
                                    showLog(String.valueOf(totalDpt));

                                    // cek Jumlah total suara presiden tidak boleh lebih besar daripada DPT
                                    if (totalDpt >= totalJumlahSuara) {
                                        // send sms
                                        sendSmsMessage(aDataVote, mUser);

                                        // user has submitted president form and save to sharedPreference
                                        mUser.setIsSubmitted("1");
                                        Application.getInstance().getPrefManager().updateUser(mUser);

                                        testGetDataFromRealm();

                                        // back to vote type
                                        startWithPop(MainFragment.newInstance());
                                    } else {
                                        showLongToast("Total Jumlah Suara : " + totalJumlahSuara +
                                                "\nDPT : " + totalDpt + "\n" +
                                                "Jumlah total suara presiden tidak boleh lebih besar daripada DPT !");
                                    }

                                    // clear jumlah data vote presiden
                                    aDataVote.clear();


                                } // end of dialog click
                            })
                            .show();

                }
            }
        });

    }

    private void showLongToast(String message) {
        Toast.makeText(_mActivity,
                message, Toast.LENGTH_LONG).show();
    }

    private void showToast(String message) {
        Toast.makeText(_mActivity, message, Toast.LENGTH_SHORT).show();
    }

    private void showKodeAndTipeRelawanAtTop() {

        // SET TIPE RELAWAN BELOW TOOLBAR
        if (mUser.getTipeRelawan() != null) {
            mTipeRelawan.setText(mUser.getTipeRelawan());
        }

        // SET ADAPTER FOR KODE LOKASI TPS
        mAdapterKodeLokasi = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataKodeLokasiCurrent
        );

        mAdapterKodeLokasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mKodeLokasi.setAdapter(mAdapterKodeLokasi);

        // set default kode lokasi tps
        String kodeLokasiDefault = aDataKodeLokasiCurrent.get(0);
        mKodeLokasi.setSelection(aDataKodeLokasiCurrent.indexOf(kodeLokasiDefault) + 1);

        mKodeLokasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void testGetDataFromRealm() {
        List<Vote> aVotes = mRealmManager.getVote("Presiden");
        for (int i = 0; i < aVotes.size(); i++) {
            // from realm db
            Log.v(TAG, aVotes.get(i).getFieldName() + " : " + aVotes.get(i).getJumlahSuara());
        }
        // from sharedpref
//        Log.v(TAG, mUser.getNama() + ", " + mUser.getKodeRelawan() + ", " + mUser.getIsSubmitted());
    }

    private void initViewFilled(View view) {
        // inflate toolbar
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // form container
        mFormLayout = view.findViewById(R.id.presiden_form_layout);

        // inflate edittext

        // add back nav to toolbar
        initToolbarNav(mToolbar);
        mToolbarTitle.setText(R.string.presiden_toolbar_title);

        // init realm manager
        String voteType = "Presiden";
        List<Vote> votes = mRealmManager.getVote(voteType);

        for (int i = 0; i < mFormLayout.getChildCount(); i++) {
            if (i < mFormLayout.getChildCount() - 1) {
                // get TextInputLayout
                TextInputLayout ITPresident = (TextInputLayout) mFormLayout.getChildAt(i);
                EditText ETPresident = ITPresident.getEditText();
                ETPresident.setText(votes.get(i).getJumlahSuara());
                ETPresident.setEnabled(false);
            }
        }
    }

    private void storeToRealmDb(String namaField, String jumlahSuara) {
        Vote vote = new Vote();
        vote.setEmail(mUser.getEmail());
        vote.setKodeTipeRelawan(mUser.getTipeRelawan());
//        vote.setKodeRelawan(mUser.getKodeRelawan());
        vote.setNamaRelawan(mUser.getNama());
        vote.setNoHp1(mUser.getNoHP1());
        vote.setNoHp2(mUser.getNoHP2());
        vote.setVoteType("Presiden");
        vote.setFieldName(namaField);
        vote.setJumlahSuara(jumlahSuara);

        // add vote data to realm db
        mRealmManager.addVote(vote);
    }


    private void sendSmsMessage(ArrayList<String> aDataJumlahSuara, User user) {

        mUser = Application.getInstance().getPrefManager().getUser();

        // get kode sms tipe relawan
        HashMap<String, String> mapKodeSmsTipeRelawan = Config.getKodeSmsTipeRelawan();
        String kodeSmsTipeRelawan = mapKodeSmsTipeRelawan.get(user.getTipeRelawan());

        // get kode lokasi TPS
        String kodeLokasiTPS = mKodeLokasi.getSelectedItem().toString();

        String longNumber = null;
        if (mUser.getLongNumber() != null) {
            longNumber = mUser.getLongNumber();
        }

        // 085714823418
        String[] destinationAddress = {
                Config.SHORT_NUMBER,
                longNumber
        };

        String scAddress = null;

        PendingIntent piSent = PendingIntent.getBroadcast(_mActivity, 32, new Intent(Config.SENT), 0);
        PendingIntent piDelivered = PendingIntent.getBroadcast(_mActivity, 33, new Intent(Config.DELIVERED), 0);

        // presiden : QP
        String smsPresiden = Config.ENTITY + " " + kodeSmsTipeRelawan + Config.QC_PRESIDEN + "#" + kodeLokasiTPS;
        for (String jumlahSuara : aDataJumlahSuara) {
            smsPresiden += "#" + jumlahSuara;
        }

        // send SMS
        for (String address : destinationAddress) {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(address, scAddress, smsPresiden, piSent, piDelivered);
        }



        Toast.makeText(_mActivity, "Input data voting presiden berhasil !", Toast.LENGTH_LONG).show();

    }

    private void showLog(String smsToSendFrom) {
        Log.e(TAG, smsToSendFrom);
    }

    private boolean isValid() {

        mUser = Application.getInstance().getPrefManager().getUser();

        if (mKodeLokasi.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Kode lokasi harus dipilih!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPresiden1.getText().toString().isEmpty() || mPresiden1.getText().toString().equalsIgnoreCase("<0")) {
            Toast.makeText(_mActivity, "Presiden 1 tidak boleh kosong atau nilai tidak boleh dibawah 0!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPresiden2.getText().toString().isEmpty() || mPresiden2.getText().toString().equalsIgnoreCase("<0")) {
            Toast.makeText(_mActivity, "Presiden 2 tidak boleh kosong atau nilai tidak boleh dibawah 0!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPresidenTidakSah.getText().toString().isEmpty() || mPresidenTidakSah.getText().toString().equalsIgnoreCase("<0")) {
            Toast.makeText(_mActivity, "Suara presiden tidak sah atau nilai tidak boleh dibawah 0!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPresidenDpt.getText().toString().isEmpty() || mPresidenTidakSah.getText().toString().equalsIgnoreCase("<0")) {
            Toast.makeText(_mActivity, "DPT tidak boleh kosong atau nilai tidak boleh dibawah 0!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mUser.getLongNumber() == null || mUser.getLongNumber().isEmpty()) {
            Toast.makeText(_mActivity, "Long number harus diisi!\nCek menu settings - Long Number", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }


}
