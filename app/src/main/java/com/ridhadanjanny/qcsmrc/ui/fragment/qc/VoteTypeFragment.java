package com.ridhadanjanny.qcsmrc.ui.fragment.qc;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;

public class VoteTypeFragment extends BaseBackFragment {

    private TextView mToolbarTitle;
    private Toolbar mToolbar;
    private CardView mPresidenBtn;
    private CardView mPartaiBtn;

    private MaterialDialog mDialog;

    public VoteTypeFragment() {
        // Required empty public constructor
    }

    public static VoteTypeFragment newInstance() {
        VoteTypeFragment fragment = new VoteTypeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vote_type, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);
        mPresidenBtn = view.findViewById(R.id.vote_presiden);
        mPartaiBtn = view.findViewById(R.id.vote_dpr);

        // activate toolbar nav : back arrow nav
        initToolbarNav(mToolbar);
        // toolbar title
        mToolbarTitle.setText(R.string.tipe_voting);

        // presiden
        mPresidenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(PresidentFragment.newInstance());
            }
        });

        // partai di DPR RI
        mPartaiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(PartaiFragment.newInstance());
            }
        });

    }

}
