package com.ridhadanjanny.qcsmrc.ui.fragment.registrasi;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.ApiEndpoint;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.Caleg;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

public class DaftarFragment extends BaseBackFragment {

    private LinearLayout mFormLayout;
    private ArrayList<Caleg> aCaleg = new ArrayList<Caleg>();
    private MaterialSpinner mTipeCaleg;
    private MaterialSpinner mDapil;
    /**
     * MATERIAL DIALOG
     */
    private MaterialDialog mDialog;

    /**
     * SPINNER : TIPE CALEG (DPR RI, DPRD, DPRD Kab/Kot)
     */
    // List to store data tipe caleg
    private List<String> aDataTipeCaleg;
    private HashMap<String, String> mDataTipeCaleg;
    private ArrayAdapter<String> mAdapterTipeCaleg;

    /**
     * SPINNER : LIST DAPIL
     */
    // List to store data tipe caleg
    private List<String> aDataDapil;
    private HashMap<String, Integer> mDataDapil;
    private ArrayAdapter<String> mAdapterDapil;

    public static DaftarFragment newInstance() {

        DaftarFragment fragment = new DaftarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_daftar, container, false);
        initView(view);
        return view;
    }

    protected void initView(View view) {
        // LinearLayout
        mFormLayout = view.findViewById(R.id.reg_form_layout);
        mTipeCaleg = view.findViewById(R.id.reg_selector_tipe_caleg);
        mDapil = view.findViewById(R.id.reg_selector_dapil);


        // load tipe caleg / surat suara
        getDataTipeCaleg();

    }

    private void addButton() {

        Button submitButton = new Button(_mActivity);
        submitButton.setId(View.generateViewId());
        submitButton.setText(R.string.submit_button);
        submitButton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        submitButton.setTextColor(getResources().getColor(R.color.white));

        // Params for button
        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );

        // set Margin for button
        buttonParams.setMargins(0,12,0,0);

        // add button in the last position within parent view : LinearLayout
        mFormLayout.addView(submitButton, mFormLayout.getChildCount(), buttonParams);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i = 0; i < mFormLayout.getChildCount(); i++) {

                    // get all of view (Edittext and TextInputLayout) before button
                    if(i < mFormLayout.getChildCount() - 1) {
                        // get TextInputLayout
                        TextInputLayout ITCaleg = (TextInputLayout) mFormLayout.getChildAt(i);
                        // get nama caleg from hint
                        String namaCaleg = ITCaleg.getHint().toString();
                        // get jumlah suara from edittext
                        String jumlahSuara = ITCaleg.getEditText().getText().toString();

                        // Create caleg object
                        Caleg caleg = new Caleg();
                        // set nama caleg
                        caleg.setNama(namaCaleg);
                        // set jumlah suara caleg
                        caleg.setJumlahSuara(jumlahSuara);

                        // add to arraylist
                        aCaleg.add(caleg);

                    }

                }

                // Convert ArrayList of Caleg object to JSON
                // for being use to store in server
                String calegJson = new Gson().toJson(aCaleg);
                // clear ArrayList
                aCaleg.clear();

                AndroidNetworking.post(ApiEndpoint.POST_SUBMIT)
                        .setTag(ApiEndpoint.DEFAULT_TAG)
                        .setPriority(Priority.MEDIUM)
                        .addBodyParameter("caleg", calegJson)
                        .build()
                        .getAsString(new StringRequestListener() {
                            @Override
                            public void onResponse(String response) {
                                    Toast.makeText(_mActivity, "Response from server: \n"
                                                    + response
                                            , Toast.LENGTH_LONG)
                                            .show();
                            }

                            @Override
                            public void onError(ANError anError) {

                            }
                        });
            }
        });

    }

    private void addEditTextCaleg(String calegName) {
        EditText ETCaleg = new EditText(_mActivity);
        ETCaleg.setId(View.generateViewId());
        // set hint using nama caleg
        ETCaleg.setHint(calegName);
        // set input type as number
        ETCaleg.setInputType(InputType.TYPE_CLASS_NUMBER);
        // Param for EditText
        LinearLayout.LayoutParams ETCalegParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        );
        ETCaleg.setLayoutParams(ETCalegParams);
        addTextInputLayout(ETCaleg, ETCalegParams);
    }

    private void addTextInputLayout(EditText ETCaleg, LinearLayout.LayoutParams ETCalegParams) {
        TextInputLayout TILayout = new TextInputLayout(_mActivity);
        TILayout.setId(View.generateViewId());
        LinearLayout.LayoutParams TILayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        TILayout.setLayoutParams(TILayoutParams);
        TILayout.addView(ETCaleg, ETCalegParams);

        // add new TextInputLayout
        mFormLayout.addView(TILayout, TILayoutParams);
    }

    private void getDataTipeCaleg() {
        AndroidNetworking.get(ApiEndpoint.GET_LIST_TYPE_CALEG)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        aDataTipeCaleg = new ArrayList<String>();
                        mDataTipeCaleg = new HashMap<String, String>();

                        for(int i = 0; i < response.length(); i++) {
                            try {
//                                Integer id = Integer.parseInt(response.getJSONObject(i).getString("id"));
                                String id = response.getJSONObject(i).getString("id");
                                String content = response.getJSONObject(i).getString("content");
                                // add nama caleg to ArrayList
                                aDataTipeCaleg.add(content);
                                // add id and nama caleg to HashMap
                                mDataTipeCaleg.put(content,id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        showTipeCalegSpinner();

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    private void showTipeCalegSpinner() {
        mAdapterTipeCaleg = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataTipeCaleg);

        mAdapterTipeCaleg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTipeCaleg.setAdapter(mAdapterTipeCaleg);
        mTipeCaleg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0) {
                    // get value of id tipe caleg by arraylist position of tipe caleg item
                    String idDapil = mDataTipeCaleg.get(aDataTipeCaleg.get(position));

                    // load data for next spinner : dapil
                    getDataDapil(idDapil);

                } else {
                    mDapil.setSelection(0);
                    mAdapterDapil.clear();
                    mAdapterDapil.notifyDataSetChanged();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mAdapterDapil  = new ArrayAdapter<String>(_mActivity, android.R.layout.simple_spinner_item, new ArrayList<String>());
        mAdapterDapil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDapil.setAdapter(mAdapterDapil);
        mDapil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0) {


                    // mTipeCaleg selected item : DPR RI, DPRD PROVINSI, DPRD KAB/KOTA
                    String tipeCalegSelected = mTipeCaleg.getSelectedItem().toString();
                    // get value : 1,2,3
                    String idTipeCaleg = mDataTipeCaleg.get(tipeCalegSelected);
                    // get dapil using position of spinner item
                    String dapil = aDataDapil.get(position);

                    // remove all child view by refresh layout
                    mFormLayout.removeAllViews();

                    // get data and show form caleg
                    getDataAndShowCalegForm(idTipeCaleg, dapil);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    /**
     * Get data dapil by id tipe caleg
     * store to arraylist and hashmap
     * @param idDapil
     */
    private void getDataDapil(String idDapil) {
        // show spinkit loader, load data dapil
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Mohon Tunggu")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.GET_LIST_DAPIL + "/" + idDapil)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // make position of dapil spinner 0
                        mDapil.setSelection(0);
                        // init arraylist for store dapil content
                        aDataDapil = new ArrayList<String>();
                        // init hashmap for store dapil id and content
                        mDataDapil = new HashMap<String, Integer>();
                        // loop array of object and store to arraylist and hashmap
                        for(int i = 0; i < response.length(); i++){
                            try {
                                // get id of dapil to help us getting parameter for next data request to spinner
                                Integer id = Integer.parseInt(response.getJSONObject(i).getString("id"));
                                // get content of dapil to store to spinner
                                String content = response.getJSONObject(i).getString("dapil");

                                // set arraylist for content of dapil spinner
                                aDataDapil.add(content);
                                // set hashmap for getting id dapil to be a param in the next url request
                                mDataDapil.put(content, id);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        mAdapterDapil.clear();
                        mAdapterDapil.addAll(aDataDapil);
                        mAdapterDapil.notifyDataSetChanged();
                        mDialog.cancel();

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void getDataAndShowCalegForm(String dapil, String tipeCaleg) {

        // show spinkit loader, load data dapil
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Mohon Tunggu")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.GET_LIST_CALEG + "/" + dapil + "/" + tipeCaleg)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for(int i = 0; i < response.length(); i++) {
                            try {
                                // get nama caleg
                                String calegName = response.getJSONObject(i).getString("caleg");
                                // add edittext within textinputlayout
                                addEditTextCaleg(calegName);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        addButton();

                        // hide spinkit when nama caleg has loaded
                        mDialog.cancel();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}
