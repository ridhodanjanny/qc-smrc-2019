package com.ridhadanjanny.qcsmrc.ui.fragment.registrasi;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.ApiEndpoint;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.Config;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.base.asd;
import com.ridhadanjanny.qcsmrc.model.KodeLokasi;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.settings.LongNumberFragment;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.User;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;


public class EditInisiasiFragment extends BaseBackFragment {

    private static final String TAG = EditInisiasiFragment.class.getSimpleName();

    // user
    private User mUser;
    private ArrayList<KodeLokasi> aKodeLokasi;
//    private KodeLokasi kodeLokasi = new KodeLokasi();

    // toolbar
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    // form inisiasi
    private RelativeLayout mLoadingContent;

    // layout
    private NestedScrollView mFormContent;
    private LinearLayout mFormFieldLayout;

    // form fill
    private MaterialSpinner mTipeRelawan;
    // kode lokasi 1 dan 2 generated by code
//    private MaterialSpinner mProv;
//    private MaterialSpinner mDapil;
//    private MaterialSpinner mKab;

    private EditText mProv;
    private EditText mDapil;
    private EditText mKab;


    private EditText mNama;
    private EditText mNoHp1;
    private EditText mNoHp2;
    private CardView mSubmit;

    // tipe relawan
    private HashMap<String, Integer> mDataTipeRelawan;
    private ArrayList<String> aDataTipeRelawan;
    private ArrayAdapter<String> mAdapterTipeRelawan;

    // kode tipe relawan
    private HashMap<String, String> mKodeTipeRelawan;

    // dialog
    private MaterialDialog mDialog;

    // realm manager
    private RealmManager mRealmManager;

    // hold index of view who will be deleted in change of tipe relawan
    private ArrayList<Integer> mChildIndex;

    /**
     * SPINNER : PILIH PROVINSI
     */
    private List<String> aDataProvinsi;
    private HashMap<Integer, String> mDataProvinsi;
    private ArrayAdapter<String> mAdapterProvinsi;

    /**
     * SPINNER : PILIH DAPIL
     */
    private List<String> aDataDapil;
    private HashMap<Integer, String> mDataDapil;
    private ArrayAdapter<String> mAdapterDapil;

    /**
     * SPINNER : PILIH KAB KOTA
     */
    private List<String> aDataKabKota;
    private HashMap<Integer, String> mDataKabKota;
    private ArrayAdapter<String> mAdapterKabKota;

    // List of data kode lokasi TPS
    private ArrayList<String> aDataKodeLokasiCurrent;
    private ArrayList<String> aDataKodeLokasi = new ArrayList<String>();

    private String mProvinsiForKab;
    private String mDapilForKab;

    // TODO: Rename and change types and number of parameters
    public static EditInisiasiFragment newInstance() {
        EditInisiasiFragment fragment = new EditInisiasiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // init User entity
        mUser = Application.getInstance().getPrefManager().getUser();

        // init realm manager
        mRealmManager = new RealmManager(_mActivity);

        aKodeLokasi = new ArrayList<KodeLokasi>();

        // get data of kode lokasi from realm db
        List<User> users = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());
        aDataKodeLokasiCurrent = new ArrayList<String>();
        for (User user : users) {
            aDataKodeLokasiCurrent.add(user.getKodeLokasi());

            KodeLokasi kodeLokasiCurrent = new KodeLokasi();
            kodeLokasiCurrent.setKodeLokasiCurrent(user.getKodeLokasi());
            aKodeLokasi.add(kodeLokasiCurrent);
        }

        // init ArrayList for new EditText for kode lokasi
        mChildIndex = new ArrayList<Integer>();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_inisiasi, container, false);
        initViewEditable(view);

        return view;

    }

    private void initViewEditable(View view) {
        // toolbar
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // loading content
        mLoadingContent = view.findViewById(R.id.in_loading);

        // layout
        mFormContent = view.findViewById(R.id.in_form);
        mFormFieldLayout = view.findViewById(R.id.in_form_field_layout);

        // primary form input
        mTipeRelawan = view.findViewById(R.id.in_type_relawan);
        // kode lokasi 1
        // kode lokasi 2

        mProv = view.findViewById(R.id.in_provinsi);
        mDapil = view.findViewById(R.id.in_dapil);
        mKab = view.findViewById(R.id.in_kab);

        mNama = view.findViewById(R.id.in_name);
        mNoHp1 = view.findViewById(R.id.in_hp);
        mNoHp2 = view.findViewById(R.id.in_hp_2);
        mSubmit = view.findViewById(R.id.in_submit);

        // init value of toolbar
        initToolbarNav(mToolbar);
        mToolbarTitle.setText(R.string.edit_inisiasi);

        // init value to form fill
        // from SharedPreference (except kode lokasi) & realm db (kode lokasi)
        mDataTipeRelawan = Config.getMapDataTipeRelawan();
        mTipeRelawan.setSelection(mDataTipeRelawan.get(mUser.getTipeRelawan()));

        // kode lokasi
        int jumlahKodeLokasi = aDataKodeLokasiCurrent.size();
        Log.d(TAG, String.valueOf(jumlahKodeLokasi) + ", " + aDataKodeLokasiCurrent.get(0));

        int position = mDataTipeRelawan.get(mUser.getTipeRelawan());

        for (String kodeLokasi : aDataKodeLokasiCurrent) {
            Log.e(TAG, kodeLokasi);
        }

        if (position == 1) {
//            showToast(mUser.getTipeRelawan());
            showEditTextForKodeLokasi(jumlahKodeLokasi, aDataKodeLokasiCurrent);
        } else if (position == 2) {
//            showToast(mUser.getTipeRelawan());
            showEditTextForKodeLokasi(jumlahKodeLokasi, aDataKodeLokasiCurrent);
        }

        mProv.setText(mUser.getProv());
        mDapil.setText(mUser.getDapil());
        mKab.setText(mUser.getKab());

        // disabled form
        EditText[] editTextDisabled = {mProv, mDapil, mKab};
        for(EditText editText : editTextDisabled) {
            editText.setEnabled(false);
        }

        mNama.setText(mUser.getNama());
        mNoHp1.setText(mUser.getNoHP1());
        mNoHp2.setText(mUser.getNoHP2());

        getDataTipeRelawan();
        //getDataProvinsi();

    }

    /**
     * GET DATA PROVINSI
     */
    private void getDataProvinsi() {
        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_PROVINSI)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        aDataProvinsi = new ArrayList<String>();
                        mDataProvinsi = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String provinsi = response.getJSONObject(i).getString("provinsi");
                                mDataProvinsi.put(i, provinsi);
                                aDataProvinsi.add(provinsi);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

//                        showForm();
                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();

                        // back to main page
                        startWithPop(MainFragment.newInstance());
                        showToast("Cek koneksi internet anda!");
                    }
                });
    }

    /**
     * GET DATA TIPE RELAWAN
     */
    private void getDataTipeRelawan() {
        // load & store data tipe relawan
        aDataTipeRelawan = Config.getListDataTipeRelawan();

        mKodeTipeRelawan = new HashMap<String, String>();
        mKodeTipeRelawan.put("Enumerator", "Q");
        mKodeTipeRelawan.put("Spotchecker", "S");


        showTipeRelawanSpinner();

        // show all form
        showForm();
    }

    private void showTipeRelawanSpinner() {

        // remove spinkit loader
        mLoadingContent.setVisibility(View.GONE);
        // add form
        mFormContent.setVisibility(View.VISIBLE);

        mAdapterTipeRelawan = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataTipeRelawan
        );

        mAdapterTipeRelawan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTipeRelawan.setAdapter(mAdapterTipeRelawan);

        mTipeRelawan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {

                    if (position == 0) {
                        removeView(mChildIndex);
                        showEditTextForKodeLokasi(1, aDataKodeLokasiCurrent);
                    } else if (position == 1) {
                        removeView(mChildIndex);
                        showEditTextForKodeLokasi(2, aDataKodeLokasiCurrent);
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    /**
     * SHOW FORM
     * After data provinsi loaded
     */
    private void showForm() {

//        // 2. PROVINSI
//        mAdapterProvinsi = new ArrayAdapter<String>(
//                _mActivity,
//                android.R.layout.simple_spinner_item,
//                aDataProvinsi
//        );
//
//        final ArrayList<Integer> arrPosition = new ArrayList<Integer>();
//        mAdapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mProv.setAdapter(mAdapterProvinsi);
//        mProv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position >= 0) {
//                    String provinsi = aDataProvinsi.get(position);
//                    mProvinsiForKab = provinsi;
//                    getDataDapil(provinsi);
//                } else {
//                    mDapil.setSelection(0);
//                    mAdapterDapil.clear();
//                    mAdapterDapil.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//
//        // set position of provinsi
////        int provPosition = aDataProvinsi.indexOf(mUser.getProv()) + 1;
////        mProv.setSelection(provPosition);
//
//        // 3. DAPIL
//        mAdapterDapil = new ArrayAdapter<String>(_mActivity, android.R.layout.simple_spinner_item, new ArrayList<String>());
//        mAdapterDapil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mDapil.setAdapter(mAdapterDapil);
//        mDapil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position >= 0) {
//                    String dapil = aDataDapil.get(position);
//                    mDapilForKab = dapil;
//                    getDataKabKota(mProvinsiForKab, mDapilForKab);
//                } else {
//                    mKab.setSelection(0);
//                    mAdapterKabKota.clear();
//                    mAdapterKabKota.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//
//        // 4. KABUPATEN & KOTA
//        mAdapterKabKota = new ArrayAdapter<String>(_mActivity, android.R.layout.simple_spinner_item, new ArrayList<String>());
//        mAdapterKabKota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        mKab.setAdapter(mAdapterKabKota);
//        mKab.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (position >= 0) {
//                    String kab = aDataKabKota.get(position);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postInisiasiNew();
            }
        });

    }

    private void postInisiasiNew() {
        if (isValidKodeLokasi() && isValid()) {

            ArrayList<User> aDataRelawan = new ArrayList<User>();

            // get kode lokasi
            for (String kodeLokasi : aDataKodeLokasi) {

                User user = new User();
                user.setEmail(mUser.getEmail());
                user.setTipeRelawan(mTipeRelawan.getSelectedItem().toString());
                user.setKodeLokasi(kodeLokasi);
//                user.setKab(mKab.getSelectedItem().toString());
                user.setProv(mUser.getProv());
                user.setDapil(mUser.getDapil());
                user.setKab(mUser.getKab());
                user.setNama(mNama.getText().toString());
                user.setNoHP1(mNoHp1.getText().toString());
                user.setNoHP2(mNoHp2.getText().toString());

                // add user to ArrayList to use later with POST API
                aDataRelawan.add(user);
            }

            // delete user from realm db
            mRealmManager.deleteUser(mUser.getEmail(), mUser.getTipeRelawan());

            // add / update new user to realm db
            for (User user : aDataRelawan) {
                mRealmManager.addUser(user);
            }

            // add user to SharedPreference
            addUserToSharedPreference(aDataRelawan);

            // add user to mysql server db via rest api
            // after successfully, back to main page

            addUserToServer(aDataRelawan);

        }

        // clear ArrayList of kodeLokasi,
        // when user changes the option of tipe relawan
        aDataKodeLokasi.clear();
    }

    private void addUserToSharedPreference(ArrayList<User> aDataRelawan) {
        // get just only 1st data in ArrayList<User>
        User userToSharedPref = aDataRelawan.get(0);
        mUser.setTipeRelawan(userToSharedPref.getTipeRelawan());
        mUser.setProv(userToSharedPref.getProv());
        mUser.setDapil(userToSharedPref.getDapil());
        mUser.setKab(userToSharedPref.getKab());
        mUser.setNama(userToSharedPref.getNama());
        mUser.setNoHP1(userToSharedPref.getNoHP1());
        mUser.setNoHP2(userToSharedPref.getNoHP2());

        // update user in SharedPreference
        Application.getInstance().getPrefManager().updateUser(mUser);
    }

    /**
     * Add user to server db
     *
     * @param aDataRelawan
     */
    private void addUserToServer(ArrayList<User> aDataRelawan) {
        String userJson = new Gson().toJson(aDataRelawan);

        AndroidNetworking.post(ApiEndpoint.POST_UPDATE_INISIASI_RELAWAN)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .addBodyParameter("user", userJson)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equalsIgnoreCase("ok")) {
                            showToast("Data relawan berhasil diupdate");
                            // after inisiasi
                            startWithPop(MainFragment.newInstance());
                        } else {
                            // remote query gagal
                            showToast(response + "\n\nData relawan gagal diupdate");
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        showToast("Data relawan gagal diupdate \n\n" + anError.getErrorDetail() +
                                "\n\nSilakan cek koneksi internet anda!");
                        Log.e(TAG, anError.getErrorDetail());
                    }
                });


    }

    private void getDataKabKota(String provinsi, String dapil) {
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Load Data Kab Kota")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_KABKOTA + "/" + provinsi + "/" + dapil)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        mKab.setSelection(0);

                        aDataKabKota = new ArrayList<String>();
                        mDataKabKota = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String dapil = response.getJSONObject(i)
                                        .getString("kab");
                                mDataKabKota.put(i, dapil);
                                aDataKabKota.add(dapil);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // refresh dapil adapter dengan data baru, sesuai provinsi yg dipilih user
                        mAdapterKabKota.clear();
                        mAdapterKabKota.addAll(aDataKabKota);
                        mAdapterKabKota.notifyDataSetChanged();

                        // stop dialog
                        mDialog.cancel();

                        // show current position of dapil
//                        int kabKotaPosition = aDataKabKota.indexOf(mUser.getKab()) + 1;
//                        mKab.setSelection(kabKotaPosition);
                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();
                    }
                });

    }

    private void getDataDapil(String provinsi) {

        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Load Data Dapil")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_DAPIL + "/" + provinsi)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mDapil.setSelection(0);
                        aDataDapil = new ArrayList<String>();
                        mDataDapil = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String dapil = response.getJSONObject(i)
                                        .getString("dapil");
                                mDataDapil.put(i, dapil);
                                aDataDapil.add(dapil);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // refresh dapil adapter dengan data baru, sesuai provinsi yg dipilih user
                        mAdapterDapil.clear();
                        mAdapterDapil.addAll(aDataDapil);
                        mAdapterDapil.notifyDataSetChanged();

                        // stop dialog
                        mDialog.cancel();

                        // show current position of dapil
//                        int dapilPosition = aDataDapil.indexOf(mUser.getDapil()) + 1;
//                        mDapil.setSelection(dapilPosition);


                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();
                    }
                });

    }


    private boolean isValidKodeLokasi() {
        if (mChildIndex.size() > 0) {
            for (int i = 0; i < mChildIndex.size(); i++) {
                TextInputLayout TIkodeLokasi = (TextInputLayout) mFormFieldLayout.getChildAt(mChildIndex.get(i));
                String hint = TIkodeLokasi.getHint().toString();
                String kodeLokasi = TIkodeLokasi.getEditText().getText().toString();
                if (kodeLokasi.isEmpty()) {
                    showToast(hint + " harus diisi");
                    return false;
                } else {
                    // add kode lokasi to ArrayList
                    aDataKodeLokasi.add(kodeLokasi);
                }
            }
        }
        return true;
    }

    private boolean isValid() {
        if (mTipeRelawan.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Tipe relawan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNama.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "Nama tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNoHp1.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "No HP 1 tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNoHp2.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "No HP 2 tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mProv.getText() == null || mProv.getText().equals("")) {
            Toast.makeText(_mActivity, "Provinsi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mDapil.getText() == null || mDapil.getText().equals("")) {
            Toast.makeText(_mActivity, "Dapil tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mKab.getText() == null || mKab.getText().equals("")) {
            Toast.makeText(_mActivity, "Kabupaten tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }


    /**
     * SHOW EDITTEXT FOR KODE LOKASI TPS
     *
     * @param loop amount of EditText
     */
    private void showEditTextForKodeLokasi(int loop, ArrayList<String> kodeLokasi) {

        int count = 0;

        if (kodeLokasi.size() < 2) {
            kodeLokasi.add("");
        }

        // i = index of view (parent : mFormFieldLayout)
        for (int i = 1; i <= loop; i++) {
            // EditText
            EditText ETKodeLokasi = new EditText(_mActivity);
            ETKodeLokasi.setId(View.generateViewId());

            // set hint
            ETKodeLokasi.setHint("Kode Lokasi " + i);
            ETKodeLokasi.setText(kodeLokasi.get(count));

            ETKodeLokasi.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
            LinearLayout.LayoutParams ETKodeLokasiParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            ETKodeLokasi.setLayoutParams(ETKodeLokasiParams);

            // TextInputLayout
            TextInputLayout TILayout = new TextInputLayout(_mActivity);
            TILayout.setId(View.generateViewId());
            LinearLayout.LayoutParams TILayoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TILayout.setLayoutParams(TILayoutParams);
            TILayout.addView(ETKodeLokasi, ETKodeLokasiParams);

            mFormFieldLayout.addView(TILayout, i);

            // add index of view yang ingin dihapus saat user mengganti tipe relawan
            mChildIndex.add(i);

            count++;
        }

    }


    /**
     * REMOVE EDITTEXT OF KODE LOKASI TPS
     * When user change tipe relawan
     *
     * @param indexToRemove
     */
    private void removeView(ArrayList<Integer> indexToRemove) {
        if (indexToRemove.size() > 0) {
            for (int i = 0; i < indexToRemove.size(); i++) {
                View view = mFormFieldLayout.getChildAt(indexToRemove.get(i));
                view.setVisibility(View.GONE);
            }
            mChildIndex.clear();
        }
    }

    private void showLog(String response) {
        Log.d(TAG, response);
    }

    private void showToast(String message) {
        Toast.makeText(_mActivity, message, Toast.LENGTH_SHORT).show();
    }


}
