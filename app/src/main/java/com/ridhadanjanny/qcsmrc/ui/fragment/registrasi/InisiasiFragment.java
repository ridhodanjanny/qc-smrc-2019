package com.ridhadanjanny.qcsmrc.ui.fragment.registrasi;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.Gson;
import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.ApiEndpoint;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.app.Config;
import com.ridhadanjanny.qcsmrc.app.RealmManager;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.Caleg;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.qc.PartaiFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.settings.LongNumberFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.settings.SettingsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

public class InisiasiFragment extends BaseBackFragment {

    /**
     * Get Name of Fragment
     */
    private static final String TAG = InisiasiFragment.class.getSimpleName();

    /**
     * User entity
     */
    private User mUser;

    /**
     * ArrayList<String> of Kode Lokasi
     */
    private ArrayList<String> aDataKodeLokasi = new ArrayList<String>();

    /**
     * SPINNER : PILIH PROVINSI
     */
    private List<String> aDataProvinsi;
    private HashMap<Integer, String> mDataProvinsi;
    private ArrayAdapter<String> mAdapterProvinsi;

    /**
     * SPINNER : PILIH DAPIL
     */
    private List<String> aDataDapil;
    private HashMap<Integer, String> mDataDapil;
    private ArrayAdapter<String> mAdapterDapil;

    /**
     * SPINNER : PILIH KAB KOTA
     */
    private List<String> aDataKabKota;
    private HashMap<Integer, String> mDataKabKota;
    private ArrayAdapter<String> mAdapterKabKota;

    /**
     * DATA ALL CALEG BY DAPIL
     */
    private ArrayList<Caleg> aDataAllCaleg;

    /**
     * Toolbar
     */
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    /**
     * Fragment Layout
     */
    private RelativeLayout mLoadingContent;
    private NestedScrollView mFormContent;
    private LinearLayout mFormFieldLayout;

    /**
     * Fragment input form : Blank Form
     */
    private MaterialSpinner mTipeRelawan;
    private MaterialSpinner mProv;
    private MaterialSpinner mDapil;
    private MaterialSpinner mKab;
    private EditText mNama;
    private EditText mNoHp1;
    private EditText mNoHp2;
    private CardView mSubmit;

    /**
     * Fragment input form : Filled-in Form
     */
    private EditText mTipeRelawanFilled;
    private EditText mKodeLokasi1Filled;
    private EditText mKodeLokasi2Filled;
    private EditText mProvFilled;
    private EditText mDapilFilled;
    private EditText mKabKotaFilled;

    /**
     * TIPE RELAWAN : List of Data Tipe Relawan & ArrayAdapter
     */
    private HashMap<String, Integer> mDataTipeRelawan;
    private ArrayList<String> aDataTipeRelawan;
    private ArrayAdapter<String> mAdapterTipeRelawan;

    /**
     * CONVERT TIPE RELAWAN TO KODE RELAWAN
     */
    private HashMap<String, String> mKodeTipeRelawan;

    /**
     * Dialog Fragment : Show dialog while fetch data for something
     */
    private MaterialDialog mDialog;

    /**
     * Realm instance for store and retrieve realm db
     */
    private RealmManager mRealmManager;

    /**
     * Hold index of view,
     * who will be removed when user change spinner of Tipe Relawan
     */
    private ArrayList<Integer> mChildIndex;

    /**
     * Hold current choice of Provinsi & Dapil
     * for the next usage in spinner (Prov, dapil, and kab kota)
     */
    private String mProvinsiForKab;
    private String mDapilForKab;


    public static InisiasiFragment newInstance() {
        InisiasiFragment fragment = new InisiasiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Init all view while fragment has begin
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;
        mUser = Application.getInstance().getPrefManager().getUser();

        // init realm manager
        mRealmManager = new RealmManager(_mActivity);

        // email user belum masuk
        if (mUser == null) {
            Toast.makeText(_mActivity, "Email pengguna tidak ditemukan", Toast.LENGTH_SHORT).show();
            pop();
        } else {
            // BLANK FORM : user belum inisiasi, blank form
            if (mUser.getNama() == null && mUser.getTipeRelawan() == null) {
                // Inflate the layout for this fragment
                view = inflater.inflate(R.layout.fragment_inisiasi, container, false);
                initView(view);
            } else {
                // DISABLED EDITTEXT : user sudah inisiasi, tampilkan value dan disabled edittext
                view = inflater.inflate(R.layout.fragment_inisiasi_filled, container, false);
                initViewFilled(view);
            }
        }
        return view;
    }

    /**
     * Initialize view of InisiasiFragment
     *
     * @param view
     */
    private void initView(View view) {

        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);
        mLoadingContent = view.findViewById(R.id.in_loading);
        mFormContent = view.findViewById(R.id.in_form);
        mFormFieldLayout = view.findViewById(R.id.in_form_field_layout);

        // primary form input
        mNama = view.findViewById(R.id.in_name);
        mProv = view.findViewById(R.id.in_provinsi);
        mDapil = view.findViewById(R.id.in_dapil);
        mKab = view.findViewById(R.id.in_kab);
        mNoHp1 = view.findViewById(R.id.in_hp);
        mNoHp2 = view.findViewById(R.id.in_hp_2);
        mTipeRelawan = view.findViewById(R.id.in_type_relawan);
        mSubmit = view.findViewById(R.id.in_submit);

        initToolbarNav(mToolbar);

        // add option to toolbar
        initToolbarOptions();

        // set text in toolbar title
        mToolbarTitle.setText(R.string.menu_2);

        // show loading spinner while fragment has started
        showLoading();

        // load data tipe relawan to use in spinner later
        getDataTipeRelawan();
        // load data provinsi
        getDataProvinsi();

    }

    /**
     * GET DATA PROVINSI
     * Connect to endpoint api to retrieve data provinsi
     * - Store data to ArrayList<String> in aDataProvinsi
     * - Store data to HashMap<Integer, String> in mDataProvinsi
     */
    private void getDataProvinsi() {
        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_PROVINSI)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        aDataProvinsi = new ArrayList<String>();
                        mDataProvinsi = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String provinsi = response.getJSONObject(i).getString("provinsi");
                                mDataProvinsi.put(i, provinsi);
                                aDataProvinsi.add(provinsi);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // load and show form inisiasi,
                        // after tipe relawan & provinsi has been loaded
                        showForm();
                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();

                        // back to main page if we fail to connect to http
                        startWithPop(MainFragment.newInstance());
                        showToast("Cek koneksi internet anda!");
                    }
                });
    }

    private void initToolbarOptions() {
        Drawable iconDrawable = ContextCompat.getDrawable(_mActivity.getApplicationContext(),
                R.drawable.ic_settings_grey_500_24dp);
        mToolbar.setOverflowIcon(iconDrawable);
        mToolbar.inflateMenu(R.menu.settings_menu);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.settings:
                        start(SettingsFragment.newInstance());
                        break;
                }

                return true;
            }
        });
    }

    private void initViewFilled(View view) {

        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);
        mLoadingContent = view.findViewById(R.id.in_loading);
        mFormContent = view.findViewById(R.id.in_form);

        // primary form input
        mTipeRelawanFilled = view.findViewById(R.id.in_type_relawan);

        mKodeLokasi1Filled = view.findViewById(R.id.in_kode_lokasi_1);
        mKodeLokasi2Filled = view.findViewById(R.id.in_kode_lokasi_2);

        mProvFilled = view.findViewById(R.id.in_provinsi_filled);
        mDapilFilled = view.findViewById(R.id.in_dapil_filled);
        mKabKotaFilled = view.findViewById(R.id.in_kabkota_filled);
        mNama = view.findViewById(R.id.in_name);
        mNoHp1 = view.findViewById(R.id.in_hp);
        mNoHp2 = view.findViewById(R.id.in_hp_2);
        mSubmit = view.findViewById(R.id.in_submit);

        initToolbarNav(mToolbar);

        // add option to toolbar
        initToolbarOptions();
        mToolbarTitle.setText(R.string.menu_2);

        // get user that has been saved in realm db before
        List<User> aUserList = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());
        ArrayList<String> aDataKodeLokasiTps = new ArrayList<String>();

        // get data kode lokasi TPS
        for (User user : aUserList) {
            aDataKodeLokasiTps.add(user.getKodeLokasi());
        }

        Log.d(TAG, String.valueOf(aDataKodeLokasiTps.size()));
        // FORM FIELD HAS BEEN FILLED BY INISIASI BEFORE
        mTipeRelawanFilled.setText(mUser.getTipeRelawan());
        // kode lokasi 1
        mKodeLokasi1Filled.setText(aDataKodeLokasiTps.get(0));

        // if kode lokasi 2 kosong, hilangkan EditText
        if (aDataKodeLokasiTps.size() > 1) {
            mKodeLokasi2Filled.setText(aDataKodeLokasiTps.get(1));
            mKodeLokasi2Filled.setEnabled(false);
        } else {
            mKodeLokasi2Filled.setVisibility(View.GONE);
        }

        mProvFilled.setText(mUser.getProv());
        mDapilFilled.setText(mUser.getDapil());
        mKabKotaFilled.setText(mUser.getKab());
        mNama.setText(mUser.getNama());
        mNoHp1.setText(mUser.getNoHP1());
        mNoHp2.setText(mUser.getNoHP2());

        // hilangkan submit button
        mSubmit.setVisibility(View.INVISIBLE);

        EditText[] editTexts = {
                mTipeRelawanFilled,
                mKodeLokasi1Filled,
                mProvFilled,
                mDapilFilled,
                mKabKotaFilled,
                mNama,
                mNoHp1,
                mNoHp2};

        disabledEditText(editTexts);

    }

    private void disabledEditText(EditText[] editText) {
        for (EditText e : editText) {
            e.setEnabled(false);
        }
    }

    /**
     * Show spinkit loader
     * until spinner content ready
     */
    private void showLoading() {
        mLoadingContent.setVisibility(View.VISIBLE);
        mFormContent.setVisibility(View.GONE);
    }

    /**
     * Get data of tipe relawan & show form
     */
    private void getDataTipeRelawan() {
        // load & store data tipe relawan
        mDataTipeRelawan = Config.getMapDataTipeRelawan();
        aDataTipeRelawan = Config.getListDataTipeRelawan();

        mKodeTipeRelawan = new HashMap<String, String>();
        mKodeTipeRelawan.put("Enumerator", "Q");
        mKodeTipeRelawan.put("Spotchecker", "S");

    }

    /**
     * Show Form of InisiasiFragment
     * this form load after we get Tipe Relawan and Data Provinsi
     */
    private void showForm() {
        // remove spinkit loader
        mLoadingContent.setVisibility(View.GONE);
        // add form
        mFormContent.setVisibility(View.VISIBLE);

        // FORM INISIASI
        // 1. TIPE RELAWAN
        // create adapter for tipe relawan
        mAdapterTipeRelawan = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataTipeRelawan
        );

        mAdapterTipeRelawan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTipeRelawan.setAdapter(mAdapterTipeRelawan);

        // store amount of new view, yang dibuat oleh user's event saat memilih tipe relawan
        mChildIndex = new ArrayList<Integer>();

        // event listener tipe relawan spinner
        mTipeRelawan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    // if position 1 : show 1 field kode lokasi for enumerator
                    // else : show 2 field kode lokasi for spotchecker
                    if (position == 0) { // position 0 : enumerator
                        removeView(mChildIndex); // remove view yang pernah dibuat sebelumnya (kalau ada)
                        showEditTextForKodeLokasi(1); // munculkan 1 edittext
                        Log.d(TAG, String.valueOf(mChildIndex.size()));
                    } else if (position == 1) { // pos 1 : spc
                        removeView(mChildIndex);
                        showEditTextForKodeLokasi(2); // munculkan 2 edittext
                        Log.d(TAG, String.valueOf(mChildIndex.size()));
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // 2. PROVINSI
        mAdapterProvinsi = new ArrayAdapter<String>(
                _mActivity,
                android.R.layout.simple_spinner_item,
                aDataProvinsi
        );

        mAdapterProvinsi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mProv.setAdapter(mAdapterProvinsi);
        mProv.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    String provinsi = aDataProvinsi.get(position);
                    mProvinsiForKab = provinsi;
                    getDataDapil(provinsi);
                } else {
                    mDapil.setSelection(0);
                    mAdapterDapil.clear();
                    mAdapterDapil.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 3. DAPIL
        mAdapterDapil = new ArrayAdapter<String>(_mActivity, android.R.layout.simple_spinner_item, new ArrayList<String>());
        mAdapterDapil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mDapil.setAdapter(mAdapterDapil);
        mDapil.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    String dapil = aDataDapil.get(position);
                    mDapilForKab = dapil;
                    getDataKabKota(mProvinsiForKab, mDapilForKab);
                } else {
                    mKab.setSelection(0);
                    mAdapterKabKota.clear();
                    mAdapterKabKota.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // 4. KABUPATEN & KOTA
        mAdapterKabKota = new ArrayAdapter<String>(_mActivity, android.R.layout.simple_spinner_item, new ArrayList<String>());
        mAdapterKabKota.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mKab.setAdapter(mAdapterKabKota);
        mKab.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {
                    String kab = aDataKabKota.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                postInisiasi();
                postInisiasiNew();
            }
        });

    }

    /**
     * Validate input of Kode Lokasi
     *
     * @return boolean
     */
    private boolean isValidKodeLokasi() {
        // check if we have a child index causes spinner `tipe relawan` click
        if (mChildIndex.size() > 0) {
            for (int i = 0; i < mChildIndex.size(); i++) {
                TextInputLayout TIkodeLokasi = (TextInputLayout) mFormFieldLayout.getChildAt(mChildIndex.get(i));
                String hint = TIkodeLokasi.getHint().toString();
                String kodeLokasi = TIkodeLokasi.getEditText().getText().toString();
                if (kodeLokasi.isEmpty()) {
                    showToast(hint + " harus diisi");
                    return false;
                } else {
                    // add kode lokasi to ArrayList
                    aDataKodeLokasi.add(kodeLokasi);
                }
            }
        }
        return true;
    }

    /**
     * Get data kab kota
     *
     * @param provinsi
     * @param dapil
     */
    private void getDataKabKota(String provinsi, String dapil) {

        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Load Data Dapil")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_KABKOTA + "/" + provinsi + "/" + dapil)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {

                        mKab.setSelection(0);

                        aDataKabKota = new ArrayList<String>();
                        mDataKabKota = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String dapil = response.getJSONObject(i)
                                        .getString("kab");
                                mDataKabKota.put(i, dapil);
                                aDataKabKota.add(dapil);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // refresh dapil adapter dengan data baru, sesuai provinsi yg dipilih user
                        mAdapterKabKota.clear();
                        mAdapterKabKota.addAll(aDataKabKota);
                        mAdapterKabKota.notifyDataSetChanged();

                        // stop dialog
                        mDialog.cancel();
                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();
                    }
                });

    }

    private void getDataDapil(String provinsi) {

        mDialog = new MaterialDialog.Builder(_mActivity)
                .content("Load Data Dapil")
                .cancelable(false)
                .progress(true, 0)
                .show();

        AndroidNetworking.get(ApiEndpoint.BASE_URL_WILAYAH_DAPIL + "/" + provinsi)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mDapil.setSelection(0);

                        aDataDapil = new ArrayList<String>();
                        mDataDapil = new HashMap<Integer, String>();
                        // loop object within JSONArray
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                String dapil = response.getJSONObject(i)
                                        .getString("dapil");
                                mDataDapil.put(i, dapil);
                                aDataDapil.add(dapil);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // refresh dapil adapter dengan data baru, sesuai provinsi yg dipilih user
                        mAdapterDapil.clear();
                        mAdapterDapil.addAll(aDataDapil);
                        mAdapterDapil.notifyDataSetChanged();

                        // stop dialog
                        mDialog.cancel();
                    }

                    @Override
                    public void onError(ANError anError) {
                        // if sistem offline
                        Log.e(TAG, anError.getErrorDetail());
                        new MaterialDialog.Builder(_mActivity)
                                .title(R.string.error_title_all)
                                .content(R.string.error_network)
                                .positiveText(R.string.btn_ok)
                                .show();
                    }
                });

    }

    private void removeView(ArrayList<Integer> indexToRemove) {
        if (indexToRemove.size() > 0) {
            for (int i = 0; i < indexToRemove.size(); i++) {
                View view = mFormFieldLayout.getChildAt(indexToRemove.get(i));
                view.setVisibility(View.GONE);
            }
            mChildIndex.clear();
        }
    }

    private void showEditTextForKodeLokasi(int loop) {

        // i = index of view (parent : mFormFieldLayout)
        for (int i = 1; i <= loop; i++) {
            // EditText
            EditText ETKodeLokasi = new EditText(_mActivity);
            ETKodeLokasi.setId(View.generateViewId());
            ETKodeLokasi.setHint("Kode Lokasi " + i);
            ETKodeLokasi.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
            LinearLayout.LayoutParams ETKodeLokasiParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);

            ETKodeLokasi.setLayoutParams(ETKodeLokasiParams);

            // TextInputLayout
            TextInputLayout TILayout = new TextInputLayout(_mActivity);
            TILayout.setId(View.generateViewId());
            LinearLayout.LayoutParams TILayoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TILayout.setLayoutParams(TILayoutParams);
            TILayout.addView(ETKodeLokasi, ETKodeLokasiParams);

            mFormFieldLayout.addView(TILayout, i);

            // add index of view yang ingin dihapus saat user mengganti tipe relawan
            mChildIndex.add(i);
        }

    }

    private boolean isValid() {
        if (mTipeRelawan.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Tipe relawan tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNama.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "Nama tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNoHp1.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "No HP 1 tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mNoHp2.getText().toString().isEmpty()) {
            Toast.makeText(_mActivity, "No HP 2 tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mProv.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Provinsi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mDapil.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Dapil tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mKab.getSelectedItem() == null) {
            Toast.makeText(_mActivity, "Kabupaten tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private void postInisiasiNew() {
        if (isValidKodeLokasi() && isValid()) {

            ArrayList<User> aDataRelawan = new ArrayList<User>();

            // remove existing user
            List<User> aUser = mRealmManager.getUser(mUser.getEmail(), mUser.getTipeRelawan());
            if (aUser.size() > 0 || !aUser.isEmpty()) {
                Log.d(TAG, String.valueOf(aUser.size()));
                mRealmManager.deleteUser(mUser.getEmail(), mUser.getTipeRelawan());
            }

            // get kode lokasi
            for (String kodeLokasi : aDataKodeLokasi) {

                User user = new User();
                user.setEmail(mUser.getEmail());
                user.setTipeRelawan(mTipeRelawan.getSelectedItem().toString());
                user.setKodeLokasi(kodeLokasi);
                user.setProv(mProv.getSelectedItem().toString());
                user.setDapil(mDapil.getSelectedItem().toString());
                user.setKab(mKab.getSelectedItem().toString());
                user.setNama(mNama.getText().toString());
                user.setNoHP1(mNoHp1.getText().toString());
                user.setNoHP2(mNoHp2.getText().toString());

                // add user to realm db
                mRealmManager.addUser(user);
                // add user to ArrayList to use later with POST API
                aDataRelawan.add(user);

            }

            /**
             * input ArrayList<User>
             * The sequence process of Inisiasi relawan :
             * 1. save data inisiasi user to realm
             * 2. update data inisiasi to SharedPreference
             * 3. post data inisiasi to mySQL server
             * 4. get data caleg by choosen dapil
             * 5. save data caleg by choosen dapil to realm db
             */

            addUserToServer(aDataRelawan);

        }

        // clear ArrayList of kodeLokasi,
        // when user changes the option of tipe relawan
        aDataKodeLokasi.clear();
    }

    private void addAllCalegToRealm(Caleg caleg) {
        mRealmManager.addCaleg(caleg);
    }

    /**
     * Get All Caleg By Dapil
     * + Get All Caleg By Choosen Dapil
     * + Store all caleg to realm db
     * + if success, we back to MainFragment
     *
     * @param dapil
     */
    public void getAllCalegByDapil(String dapil) {

        // create dialog when process started
        mDialog = new MaterialDialog.Builder(_mActivity)
                .content(R.string.please_wait)
                .progress(true, 0)
                .cancelable(false)
                .show();

        AndroidNetworking.post(ApiEndpoint.GET_LIST_DATA_CALEG + "/" + dapil)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mDialog.cancel();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                // get field
                                String prov = response.getJSONObject(i).getString("prov");
                                String dapil = response.getJSONObject(i).getString("dapil");
                                String namaPartai = response.getJSONObject(i).getString("nama_partai");
                                String namaCaleg = response.getJSONObject(i).getString("caleg");

                                Caleg caleg = new Caleg();
                                caleg.setProv(prov);
                                caleg.setDapil(dapil);
                                caleg.setPartai(namaPartai);
                                caleg.setNama(namaCaleg);

                                // add data caleg by dapil to realm local db
                                mRealmManager.addCaleg(caleg);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        // after all process has successful, go to MainFragment
                        startWithPop(MainFragment.newInstance());

                    }

                    @Override
                    public void onError(ANError anError) {

                        showToast(anError.getErrorDetail() + "\n\n"
                                + "Get data caleg belum berhasil. \nSilakan cek koneksi internet anda !");

                    }
                });

    }

    /**
     * Inisiasi Relawan : Update SharedPreference of User
     *
     * @param aDataRelawan
     * @param longNumber
     */
    private void addUserToSharedPreference(ArrayList<User> aDataRelawan, String longNumber) {

        // get just only 1st data in ArrayList<User>
        User userToSharedPref = aDataRelawan.get(0);

        // update User in the SharedPreference
        mUser.setTipeRelawan(userToSharedPref.getTipeRelawan());
        mUser.setProv(userToSharedPref.getProv());
        mUser.setDapil(userToSharedPref.getDapil());
        mUser.setKab(userToSharedPref.getKab());
        mUser.setNama(userToSharedPref.getNama());
        mUser.setNoHP1(userToSharedPref.getNoHP1());
        mUser.setNoHP2(userToSharedPref.getNoHP2());

        // Set long number to SharedPreference, default
        if (!longNumber.equals("") || longNumber != null) {
            mUser.setLongNumber(longNumber);
        } else {
            mUser.setLongNumber("085884725424"); // default long number, if we get null of longNumber
        }

        // update user in SharedPreference
        Application.getInstance().getPrefManager().updateUser(mUser);

    }


    /**
     * Inisiasi Relawan Process:
     * + Add User to SQL Server
     * + Get long number by dapil we had choosen before
     * + Update SharedPreference of User
     * + Get data caleg 19 dapil & store to realm db
     *
     * @param aDataRelawan
     */
    private void addUserToServer(final ArrayList<User> aDataRelawan) {

        String userJson = new Gson().toJson(aDataRelawan);

        AndroidNetworking.post(ApiEndpoint.POST_SUBMIT_INISIASI_RELAWAN)
                .setTag(ApiEndpoint.DEFAULT_TAG)
                .setPriority(Priority.MEDIUM)
                .addBodyParameter("user", userJson)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            // if status = ok
                            if (response.getString("status").equals("ok")) {
                                // show long number to user
                                showToast("Your long number is " + response.getString("body"));
                                // set longNumber to new var
                                String longNumber = response.getString("body");

                                Log.d(TAG, longNumber);

                                // update SharedPreference of User
                                addUserToSharedPreference(aDataRelawan, longNumber);
                                // get all data caleg by dapil from server to realm db
                                getAllCalegByDapil(mUser.getDapil());
                            } else {
                                // show error status to user as toast
                                showToast(response.getString("body"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, anError.getErrorBody() + ", " + anError.getErrorCode() + "," + anError.getResponse() + "," + anError.getMessage());
                        showToast(anError.getErrorDetail() + "\n\n"
                                + "Inisiasi belum berhasil. \nSilakan cek koneksi internet anda !");

                    }
                });


    }


    private void showLog(String user) {
        Log.d(TAG, user);
    }

    private void showToast(String msg) {
        Toast.makeText(_mActivity, msg, Toast.LENGTH_SHORT).show();
    }

}
