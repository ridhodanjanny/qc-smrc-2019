package com.ridhadanjanny.qcsmrc.ui.fragment.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;

public class LongNumberFragment extends BaseBackFragment {

    // toolbar
    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private User mUser;

    // long num blank form
    private EditText mLongNum;

    // long num filled & disabled
    private EditText mActiveLongNum;
    private LinearLayout mActiveLongNumLayout;

    // submit button
    private LinearLayout mSubmit;

    private LinearLayout mBackToHomeButton;

    public static LongNumberFragment newInstance() {
        LongNumberFragment fragment = new LongNumberFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;
        view = inflater.inflate(R.layout.fragment_long_number, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mToolbar = view.findViewById(R.id.toolbar);
        mToolbarTitle = view.findViewById(R.id.toolbar_title);

        // long num
        mLongNum = view.findViewById(R.id.long_number);

        // active long number
        mActiveLongNumLayout = view.findViewById(R.id.long_number_active_layout);
        mActiveLongNum = view.findViewById(R.id.long_number_active);

        // submit button
        mSubmit = view.findViewById(R.id.update_long_number_button);

        // back to home button
        mBackToHomeButton = view.findViewById(R.id.long_number_back_home);

        // init toolbar
        initToolbarNav(mToolbar);
        mToolbarTitle.setText("Long Number");
        showToast("Silakan isi long number terlebih dahulu.");

        mUser = Application.getInstance().getPrefManager().getUser();

        showHomeButton();

        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                if(isValid()) {
                    String longNumber = mLongNum.getText().toString();

                    // update long number storage
                    mUser.setLongNumber(longNumber);
                    Application.getInstance().getPrefManager().updateUser(mUser);

                    // visible active long number layout
                    mActiveLongNumLayout.setVisibility(View.VISIBLE);
                    mActiveLongNum.setText(longNumber);

                    // show home button
                    showHomeButton();

                    showToast("Long number berhasil diupdate");
                    // clear form again
                    mLongNum.setText("");

                    // hide keyboard
                    hideSoftInput();
                }

            }
        });

        mBackToHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWithPop(MainFragment.newInstance());
                showToast("Silakan ke menu Presiden dan Partai untuk mengisi data Quick Count");
            }
        });

    }

    private void showHomeButton() {
        /**
         * LAST CHECKPOINT : ADD ACTIVE LONG NUMBER
         */
        if(mUser.getLongNumber() != null) {
            // visible back to home button
            mBackToHomeButton.setVisibility(View.VISIBLE);
            // visible layout of active long number
            mActiveLongNumLayout.setVisibility(View.VISIBLE);
            mActiveLongNum.setText(mUser.getLongNumber());
        }
    }

    private boolean isValid() {
        if(mLongNum.getText().toString().isEmpty()) {
            showToast("Long number harus diisi");
            return false;
        }
        return true;
    }


    private void showToast(String longNumber) {
        Toast.makeText(_mActivity, longNumber
                , Toast.LENGTH_SHORT).show();
    }

}
