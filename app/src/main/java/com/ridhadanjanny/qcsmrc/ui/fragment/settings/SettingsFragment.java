package com.ridhadanjanny.qcsmrc.ui.fragment.settings;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ridhadanjanny.qcsmrc.R;
import com.ridhadanjanny.qcsmrc.app.Application;
import com.ridhadanjanny.qcsmrc.base.BaseBackFragment;
import com.ridhadanjanny.qcsmrc.model.User;
import com.ridhadanjanny.qcsmrc.ui.fragment.MainFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.registrasi.EditInisiasiFragment;
import com.ridhadanjanny.qcsmrc.ui.fragment.registrasi.InisiasiFragment;


public class SettingsFragment extends BaseBackFragment {

    private static final String TAG = SettingsFragment.class.getSimpleName();
    // toolbar
    private Toolbar mToolbar;
    private TextView mTitle;

    // LinearLayout button
    private LinearLayout mEditRelawan;
    private LinearLayout mEditLongNum;

    // User model
    private User mUser;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = null;
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        // init default view to settings
        initView(view);

        return view;

    }

    private void initView(View view) {
        mToolbar = view.findViewById(R.id.toolbar);
        mTitle = view.findViewById(R.id.toolbar_title);
        mTitle.setText("Settings");
        initToolbarNav(mToolbar);

        mEditRelawan = view.findViewById(R.id.edit_relawan);
        mEditLongNum = view.findViewById(R.id.long_number_relawan);

        mUser = Application.getInstance().getPrefManager().getUser();

        showLog(mUser.getEmail() + ", " + mUser.getIsEdited());

        mEditRelawan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showToast("edit inisiasi");

                if(mUser.getTipeRelawan() == null || mUser.getNama().isEmpty()) {
                    showToast("Anda belum melakukan inisiasi. Silakan inisiasi terlebih dahulu");
                    startWithPop(InisiasiFragment.newInstance());
                } else {
                    start(EditInisiasiFragment.newInstance());
                }

            }
        });

        mEditLongNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start(LongNumberFragment.newInstance());
            }
        });

    }

    private void showLog(String isEdited) {
        Log.v(TAG, isEdited);
    }

    private void showToast(String msg) {
        Toast.makeText(_mActivity, msg, Toast.LENGTH_SHORT).show();
    }


}
